/*
 *  Copyright (c) 2011 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; see the file COPYING.RUNTIME.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 *
 * The file COPYING.RUNTIME.EXCEPTION contains an exception that allow
 * to use this file with other open source license
 */

#include "Asserts.h"

#include <cmath>
#include <fstream>

#include "Reporter.h"
#include <complex>

void Cauchy::xUnit::assertTrue(bool v, const std::string& msg, int line)
{
  if(v)
  {
    Reporter::instance()->reportSuccess(msg, line);
  } else {
    Reporter::instance()->reportFailure(msg, line);
  }
}

inline bool eof(std::ifstream& stream)
{
  return stream.eof() or stream.fail() or stream.bad();
}

void Cauchy::xUnit::assertFilesEqual(const std::string& file1, const std::string& file2, const std::string& msg, int line)
{
  std::ifstream stream1(file1.c_str());
  std::ifstream stream2(file2.c_str());
  bool same = true;
  while(not eof(stream1) and not eof(stream2))
  {
    if(stream1.get() != stream2.get())
    {
      same = false;
      break;
    }
  }
  assertTrue(same and eof(stream1) and eof(stream2), msg, line);
}

void Cauchy::xUnit::assertElementsAlmostEqual(double x, double y, double t, const std::string& msg, int line)
{
  assertTrue(std::fabs(x-y) < t, msg, line);
}

void Cauchy::xUnit::assertElementsAlmostEqual(const std::complex<double>& x, const std::complex<double>& y, double t, const std::string& msg, int line)
{
  assertElementsAlmostEqual(x.real(), y.real(), t, msg, line);
  assertElementsAlmostEqual(x.imag(), y.imag(), t, msg, line);
}

void Cauchy::xUnit::assertElementsAlmostEqual(int x, int y, double t, const std::string& msg, int line)
{
  assertTrue(std::fabs(x-y) < t, msg, line);
}
