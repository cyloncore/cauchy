/*
 *  Copyright (c) 2010 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; see the file COPYING.RUNTIME.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 *
 * The file COPYING.RUNTIME.EXCEPTION contains an exception that allow
 * to use this file with other open source license
 */

#ifndef _CAUCHY_EIGEN3_H_
#define _CAUCHY_EIGEN3_H_

#include <Eigen/Eigen>

// If there is no CAUCHY Definitions, then default to using double.
#ifndef _CAUCHY_DEFINITIONS_
#define _CAUCHY_DEFINITIONS_
  namespace Cauchy {
    typedef Eigen::MatrixXd Matrix;
    typedef double Number;
  }
  #define Number Cauchy::Number
#endif

#include "Eigen3/ComplexOperators.h"
#include "Eigen3/MatrixOperators.h"

#include "Eigen3/Complex.h"

#include "Eigen3/Display.h"
#include "Eigen3/Eig.h"
#include "Eigen3/FFT.h"
#include "Eigen3/LU.h"
#include "Eigen3/Matrix.h"
#include "Eigen3/Range.h"
#include "Eigen3/RangeIterator.h"
#include "Eigen3/MatrixIterator.h"
#include "Eigen3/System.h"
#include "Eigen3/Time.h"
#include "Eigen3/Unknown.h"

#include "Eigen3/Octave.h"

#endif
