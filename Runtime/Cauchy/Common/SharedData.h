/*
 *  Copyright (c) 2015 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; see the file COPYING.RUNTIME.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 *
 * The file COPYING.RUNTIME.EXCEPTION contains an exception that allow
 * to use this file with other open source license
 */

#ifndef _CAUCHY_SHARED_DATA_HPP_
#define _CAUCHY_SHARED_DATA_HPP_

#include <atomic>
#include "Debug.h"

namespace Cauchy
{
  class SharedData {
      SharedData(const SharedData&);
      const SharedData& operator=(const SharedData&);
    protected:
      SharedData() : m_count(0), m_weakCount(0)
      {
      }
      ~SharedData()
      {
        CAUCHY_ASSERT(m_count == 0);
        if(m_weakCount and std::atomic_fetch_add(m_weakCount, -1) == 1)
        {
          delete m_weakCount;
        }
      }
    public:
      void ref()
      {
        ++m_count;
      }
      int deref()
      {
        return --m_count;
      }
      int count() const
      {
        return m_count;
      }
      std::atomic<int>* weakCounter()
      {
        if(not m_weakCount)
        {
          m_weakCount = new std::atomic<int>();
          *m_weakCount = 1;
        }
        return m_weakCount;
      }
      
    private:
      std::atomic<int> m_count;
      std::atomic<int>* m_weakCount;
  };
  
}

#endif
