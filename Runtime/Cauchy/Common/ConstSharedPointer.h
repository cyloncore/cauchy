/*
 *  Copyright (c) 2015 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; see the file COPYING.RUNTIME.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 *
 * The file COPYING.RUNTIME.EXCEPTION contains an exception that allow
 * to use this file with other open source license
 */

#ifndef _CAUCHY_CONSTSHAREDPOINTER_P_H_
#define _CAUCHY_CONSTSHAREDPOINTER_P_H_

namespace Cauchy {
  template<typename T>
  class ConstSharedPointer {
    template<class>
    friend class ConstSharedPointer;
    public:
      ConstSharedPointer(const T* _t = nullptr) : t(_t)
      {
        ref();
      }
      template<template<class T2> class S >
      ConstSharedPointer(const S<T>& _t) : t(_t.cdata())
      {
        ref();
      }
      ConstSharedPointer<T>& operator=(const ConstSharedPointer<T>& _rhs)
      {
        deref();
        t = _rhs.t;
        ref();
        return *this;
      }
      ConstSharedPointer( const ConstSharedPointer<T>& _rhs ) : t(_rhs.t)
      {
        ref();
      }
      template<typename T2, template<typename> class T3, typename = typename
               std::enable_if<std::is_convertible<T2*, T*>::value>::type>
      ConstSharedPointer( const T3<T2>& _rhs ) : t(_rhs.t)
      {
        ref();
      }
      template<typename T2, template<typename> class T3, typename = typename
               std::enable_if<std::is_convertible<T2*, T*>::value>::type>
      ConstSharedPointer<T>& operator=( const T3<T2>& _rhs )
      {
        deref();
        t = _rhs.t;
        ref();
        return *this;
      }
      ~ConstSharedPointer()
      {
        deref();
      }
      const T* operator->() const {
        return t;
      }
      const T& operator*() const {
        return *t;
      }
      operator bool() const {
        return t;
      }
      template<typename T2, typename = typename
               std::enable_if<std::is_convertible<T2*, T*>::value>::type>
      bool operator==(const T2* _t2) const
      {
        return t == _t2;
      }
      template<typename T2, template<typename> class T3, typename = typename
               std::enable_if<std::is_convertible<T2*, T*>::value>::type>
      bool operator==(const T3<T2>& _t2) const
      {
        return t == _t2.t;
      }
      template<typename T2, template<typename> class T3, typename = typename
               std::enable_if<std::is_convertible<T2*, T*>::value>::type>
      bool operator!=(const T3<T2>& _t2) const
      {
        return t != _t2.t;
      }
      template<typename T2, typename = typename
               std::enable_if<std::is_convertible<T2*, T*>::value>::type>
      bool operator!=(const T2* _t2) const
      {
        return t != _t2;
      }
      template<typename T2>
      ConstSharedPointer<T2> scast() const {
        return ConstSharedPointer<T2>( static_cast<const T2*>(t) );
      }
      template<typename T2>
      ConstSharedPointer<T2> dcast() const {
        return ConstSharedPointer<T2>( dynamic_cast<const T2*>(t) );
      }
      const T* cdata() const { return t; }
    private:
      T* data() { return const_cast<T*>(t); }
      void ref()
      {
        if(t)
        {
          data()->ref();
        }
      }
      void deref()
      {
        if(t && data()->deref() == 0)
        {
          delete t;
        }
        t = 0;
      }
      const T* t;
  };
  template<typename T>
  bool operator==(const T* _t1, const ConstSharedPointer<T>& _t2)
  {
    return _t2 == _t1;
  }
};

#endif
