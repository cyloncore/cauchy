/*
 *  Copyright (c) 2015 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; see the file COPYING.RUNTIME.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 *
 * The file COPYING.RUNTIME.EXCEPTION contains an exception that allow
 * to use this file with other open source license
 */

#ifndef _CAUCHY_SHAREDPOINTER_P_H_
#define _CAUCHY_SHAREDPOINTER_P_H_

#include "ConstSharedPointer.h"

namespace Cauchy {
  template<typename T>
  class SharedPointer : public ConstSharedPointer<T> {
      template<class>
      friend class SharedPointer;
    public:
      SharedPointer(T* _t = nullptr) : ConstSharedPointer<T>(_t)
      {
      }
      SharedPointer<T>& operator=(const SharedPointer<T>& _rhs)
      {
        *(ConstSharedPointer<T>*)(this) = _rhs;
      }
      SharedPointer( const SharedPointer<T>& _rhs ) : ConstSharedPointer<T>(_rhs) {
      }
      ~SharedPointer()
      {
      }
      template<typename T2, typename = typename
               std::enable_if<std::is_convertible<T2*, T*>::value>::type>
      SharedPointer( const SharedPointer<T2>& _rhs ) : ConstSharedPointer<T>(_rhs.cdata())
      {
      }
      template<typename T2, typename = typename
               std::enable_if<std::is_convertible<T2*, T*>::value>::type>
      SharedPointer<T>& operator=( const SharedPointer<T2>& _rhs )
      {
        *(static_cast<ConstSharedPointer<T>* >(this) ) = _rhs;
        return *this;
      }
      template<typename T2, typename = typename std::enable_if<std::is_convertible<T*, T2*>::value>::type>
      operator ConstSharedPointer<T2>( )
      {
        return ConstSharedPointer<T2>(data());
      }
      T* operator->() {
        return data();
      }
      T& operator*() {
        return *data();
      }
      const T* operator->() const {
        return this->cdata();
      }
      const T& operator*() const {
        return *this->cdata();
      }
      template<typename T2>
      SharedPointer<T2> scast() {
        return SharedPointer<T2>( static_cast<T2*>(data()) );
      }
      template<typename T2>
      SharedPointer<T2> dcast() {
        return SharedPointer<T2>( dynamic_cast<T2*>(data()) );
      }
    protected:
      T* data() { return const_cast<T*>(this->cdata()); }
  };
};

#endif
