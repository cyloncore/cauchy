/*
 *  Copyright (c) 2015 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; see the file COPYING.RUNTIME.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 *
 * The file COPYING.RUNTIME.EXCEPTION contains an exception that allow
 * to use this file with other open source license
 */

#ifndef _CAUCHY_WEAKSHAREDPOINTER_P_H_
#define _CAUCHY_WEAKSHAREDPOINTER_P_H_

#include "ConstWeakSharedPointer.h"

namespace Cauchy {
  template<typename T>
  class WeakSharedPointer : public ConstWeakSharedPointer<T> {
    public:
      WeakSharedPointer(T* _t = nullptr) : ConstWeakSharedPointer<T>(_t)
      {
      }
      WeakSharedPointer<T>& operator=(const WeakSharedPointer<T>& _rhs)
      {
        *(ConstWeakSharedPointer<T>*)(this) = _rhs;
      }
      WeakSharedPointer( const WeakSharedPointer<T>& _rhs ) : ConstWeakSharedPointer<T>(_rhs) {
      }
      ~WeakSharedPointer()
      {
      }
      T* operator->() {
        return data();
      }
      T& operator*() {
        return *data();
      }
      template<typename T2>
      SharedPointer<T2> scast() {
        return SharedPointer<T2>( static_cast<T2*>(data()) );
      }
      template<typename T2>
      SharedPointer<T2> dcast() {
        return SharedPointer<T2>( dynamic_cast<T2*>(data()) );
      }
      T* data() { return const_cast<T*>(this->cdata()); }
  };
};

#endif
