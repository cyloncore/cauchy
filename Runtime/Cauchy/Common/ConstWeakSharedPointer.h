/*
 *  Copyright (c) 2015 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; see the file COPYING.RUNTIME.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 *
 * The file COPYING.RUNTIME.EXCEPTION contains an exception that allow
 * to use this file with other open source license
 */

#ifndef _CAUCHY_CONSTWEAKSHAREDPOINTER_P_H_
#define _CAUCHY_CONSTWEAKSHAREDPOINTER_P_H_

#include<atomic>
#include "Debug.h"

namespace Cauchy {
  template<typename T>
  class ConstWeakSharedPointer {
    public:
      ConstWeakSharedPointer(const T* _t = nullptr) : t(0), m_weakCount(0)
      {
        attach(_t);
      }
      ConstWeakSharedPointer<T>& operator=(const ConstWeakSharedPointer<T>& _rhs)
      {
        detach();
        attach(_rhs.t);
        return *this;
      }
      template<template<class T2> class S >
      ConstWeakSharedPointer(const S<T>& _t) : t(0), m_weakCount(0)
      {
        attach(_t.cdata());
      }
      ConstWeakSharedPointer( const ConstWeakSharedPointer<T>& _rhs ) : t(0), m_weakCount(0) {
        attach(_rhs.t);
      }
      ~ConstWeakSharedPointer()
      {
        detach();
      }
      const T* operator->() const {
        return t;
      }
      const T& operator*() const {
        return *t;
      }
      operator bool() const {
        return t;
      }
      template<typename T2, typename = typename
               std::enable_if<std::is_convertible<T2*, T*>::value>::type>
      bool operator==(const T2* _t2) const
      {
        return t == _t2;
      }
      template<typename T2, template<typename> class T3,  typename = typename
               std::enable_if<std::is_convertible<T2*, T*>::value>::type>
      bool operator==(const T3<T2>& _t2) const
      {
        return t == _t2.t;
      }
      template<typename T2, template<typename> class T3,  typename = typename
               std::enable_if<std::is_convertible<T2*, T*>::value>::type>
      bool operator!=(const T3<T2>& _t2) const
      {
        return t != _t2.t;
      }
      template<typename T2, typename = typename
               std::enable_if<std::is_convertible<T2*, T*>::value>::type>
      bool operator!=(const T2* _t2) const
      {
        return t != _t2;
      }
      template<typename T2>
      ConstWeakSharedPointer<T2> scast() const {
        return ConstWeakSharedPointer<T2>( static_cast<const T2*>(t) );
      }
      template<typename T2>
      ConstWeakSharedPointer<T2> dcast() const {
        return ConstWeakSharedPointer<T2>( dynamic_cast<const T2*>(t) );
      }
      const T* cdata() const { return t; }
    private:
      T* data() { return const_cast<T*>(t); }
      void attach(const T* new_t)
      {
        t = new_t;
        if(t)
        {
          m_weakCount = data()->weakCounter();
          *(m_weakCount) += 2;
        }
      }
      void detach()
      {
        t = 0;
        if(m_weakCount and ( std::atomic_fetch_add(m_weakCount, -2) <= 2 ))
        {
          CAUCHY_ASSERT(m_weakCount == 0);
          delete m_weakCount;
        }
        m_weakCount = 0;
      }
      const T* t;
      std::atomic<int>* m_weakCount;
  };
};

#endif
