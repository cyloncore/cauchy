/*
 *  Copyright (c) 2015 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; see the file COPYING.RUNTIME.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 *
 * The file COPYING.RUNTIME.EXCEPTION contains an exception that allow
 * to use this file with other open source license
 */

#ifndef _CAUCHY_SHARED_POINTER_DECLARATION_H_
#define _CAUCHY_SHARED_POINTER_DECLARATION_H_

namespace Cauchy
{
  template<typename T>
  class SharedPointer;
  template<typename T>
  class ConstSharedPointer;

  template<typename T>
  class WeakSharedPointer;
  template<typename T>
  class ConstWeakSharedPointer;
}

#define CAUCHY_DECLARE_SHARED_POINTERS(_CLASS_NAME_)                            \
  class _CLASS_NAME_;                                                         \
  typedef Cauchy::SharedPointer<_CLASS_NAME_> _CLASS_NAME_ ## SP;               \
  typedef Cauchy::WeakSharedPointer<_CLASS_NAME_> _CLASS_NAME_ ## WSP;          \
  typedef Cauchy::ConstSharedPointer<_CLASS_NAME_> _CLASS_NAME_ ## CSP;         \
  typedef Cauchy::ConstWeakSharedPointer<_CLASS_NAME_> _CLASS_NAME_ ## CWSP;

#endif
  