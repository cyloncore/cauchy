/*
 *  Copyright (c) 2010 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; see the file COPYING.RUNTIME.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 *
 * The file COPYING.RUNTIME.EXCEPTION contains an exception that allow
 * to use this file with other open source license
 */

#ifndef _CAUCHY_CONFIG_H_
#define _CAUCHY_CONFIG_H_

/**
 *  The operating system, must be one of: (CAUCHY_OS_x)
 *
 *    WINDOWS   - Windows
 *    LINUX     - Linux
 *    SOLARIS   - Sun Solaris
 *    MAC       - Mac OS X
 *    UNIX      - Any Unix (including Linux, Solaris or MacOSX)
 */

#if defined(__APPLE__) && (defined(__GNUC__) || defined(__xlC__) || defined(__xlc__))
#  define CAUCHY_OS_MAC
#  define CAUCHY_OS_UNIX
#elif !defined(SAG_COM) && (defined(WIN64) || defined(_WIN64) || defined(__WIN64__))
#  define CAUCHY_OS_WINDOWS
#elif !defined(SAG_COM) && (defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__))
#  define CAUCHY_OS_WINDOWS
#elif defined(__MWERKS__) && defined(__INTEL__)
#  define CAUCHY_OS_WINDOWS
#elif defined(__sun) || defined(sun)
#  define CAUCHY_OS_SOLARIS
#elif defined(hpux) || defined(__hpux)
#  define CAUCHY_OS_UNIX
#elif defined(__ultrix) || defined(ultrix)
#  define CAUCHY_OS_UNIX
#elif defined(__linux__) || defined(__linux)
#  define CAUCHY_OS_LINUX
#  define CAUCHY_OS_UNIX
#elif defined(__FreeBSD__) || defined(__DragonFly__)
#  define CAUCHY_OS_UNIX
#elif defined(__NetBSD__)
#  define CAUCHY_OS_UNIX
#elif defined(__OpenBSD__)
#  define CAUCHY_OS_UNIX
#elif defined(__bsdi__)
#  define CAUCHY_OS_UNIX
#elif defined(__sgi)
#  define CAUCHY_OS_UNIX
#elif defined(_AIX)
#  define CAUCHY_OS_UNIX
#elif defined(__GNU__)
#  define CAUCHY_OS_UNIX
#elif defined(__DGUX__)
#  define CAUCHY_OS_UNIX
#elif defined(__QNXNTO__)
#  define CAUCHY_OS_UNIX
#elif defined(__svr4__)
#  define CAUCHY_OS_UNIX
#elif defined(__MAKEDEPEND__)
#else
#  error "Unknown OS"
#endif

#if defined(__x86_64__) || defined(_M_X64)
#  define CAUCHY_ARCH_X86_64
#elif defined(__i386__) || defined(_X86_) || defined(_M_IX86)
#  define CAUCHY_ARCH_X86_32
#else
#  error "Unknown architecture"
#endif

#ifdef __APPLE__
# ifdef __BIG_ENDIAN__
#  define WORDS_BIGENDIAN 1
# else
#  undef WORDS_BIGENDIAN
# endif
#else
/* Define to 1 if your processor stores words with the most significant byte
   first (like Motorola and SPARC, unlike Intel and VAX). */
#cmakedefine WORDS_BIGENDIAN ${CMAKE_WORDS_BIGENDIAN}
#endif

#if WORDS_BIGENDIAN
#  define CAUCHY_BIGENDIAN
#else
#  define CAUCHY_LITTLEENDIAN
#endif

#undef WORDS_BIGENDIAN

#endif
