/*
 *  Copyright (c) 2010 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; see the file COPYING.RUNTIME.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 *
 * The file COPYING.RUNTIME.EXCEPTION contains an exception that allow
 * to use this file with other open source license
 */

#ifndef _CAUCHY_SYSTEM_H_
#define _CAUCHY_SYSTEM_H_

#include <string>
#include <Cauchy/Config.h>

namespace Cauchy {
  std::string computer()
  {

#if   defined(CAUCHY_OS_WINDOWS)
#  if   defined(CAUCHY_ARCH_X86_64)
    return "PCWIN64";
#  elif defined(CAUCHY_ARCH_X86_32)
    return "PCWIN";
#  endif
#elif defined(CAUCHY_OS_LINUX)
#  if   defined(CAUCHY_ARCH_X86_64)
    return "GLNXA64";
#  elif defined(CAUCHY_ARCH_X86_32)
    return "GLNX86";
#  endif
#elif defined(CAUCHY_OS_MAC)
#  if   defined(CAUCHY_ARCH_X86_64)
    return "MACI64";
#  elif defined(CAUCHY_ARCH_X86_32)
    return "MACI";
#  endif
#elif defined(CAUCHY_OS_SOLARIS)
#  if   defined(CAUCHY_ARCH_X86_64)
    return "SOL64";
#  elif defined(CAUCHY_ARCH_X86_32)
    return "SOL32";
#  endif
#else
#  if   defined(CAUCHY_ARCH_X86_64)
    return "OTHER64";
#  elif defined(CAUCHY_ARCH_X86_32)
    return "OTHER32";
#  endif
#endif
  }
  std::string computer(Number* maxsize)
  {
    *maxsize = 4294967295UL;
    return computer();
  }
  std::string computer(Number* maxsize, std::string* endian)
  {
#ifdef CAUCHY_LITTLEENDIAN
    *endian = "L";
#elif defined(CAUCHY_BIGENDIAN)
    *endian = "B";
#endif
    return computer(maxsize);
  }
  bool ispc()
  {
#ifdef CAUCHY_OS_WINDOWS
    return true;
#else
    return false;
#endif
  }
  bool isunix()
  {
#ifdef CAUCHY_OS_UNIX
    return true;
#else
    return false;
#endif
  }
  bool ismac()
  {
#ifdef CAUCHY_OS_MAC
    return true;
#else
    return false;
#endif
  }
}

#endif
