/*
 *  Copyright (c) 2010,2015 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; see the file COPYING.RUNTIME.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 *
 * The file COPYING.RUNTIME.EXCEPTION contains an exception that allow
 * to use this file with other open source license
 */

#include <cmath>
#include <cstdlib>

#ifndef _CAUCHY_EIGEN3_MATRIX_H_
#define _CAUCHY_EIGEN3_MATRIX_H_

namespace Cauchy {
  inline double pow(double a, double b)
  {
    return std::pow(a, b);
  }
  inline double pow(int a, double b)
  {
    return std::pow(a, b);
  }
  inline double pow(double a, int b)
  {
    return std::pow(a, b);
  }
  inline double pow(int a, int b)
  {
    return std::pow(a, b);
  }
  template<typename _T_>
  inline Eigen::Matrix<_T_, Eigen::Dynamic, Eigen::Dynamic> pow(Eigen::Matrix<_T_, Eigen::Dynamic, Eigen::Dynamic> a, int b)
  {
    Eigen::MatrixXd r = a;
    for(int i = 0; i < b; ++i)
    {
      r *= a;
    }
    return r;
  }
  template<typename _T_>
  inline Eigen::Matrix<_T_, Eigen::Dynamic, Eigen::Dynamic> pow(Eigen::Matrix<_T_, Eigen::Dynamic, Eigen::Dynamic> /*a*/, double /*b*/)
  {
    abort();
  }
  template<typename _T_>
  inline Eigen::Matrix<_T_, Eigen::Dynamic, Eigen::Dynamic> pow(double /*a*/, Eigen::Matrix<_T_, Eigen::Dynamic, Eigen::Dynamic> /*b*/)
  {
    abort();
    // 2 ^ M == V* (D^2) * V with [V, D] = eigs(M)
  }
  template<typename _T_>
  inline Eigen::Matrix<_T_, Eigen::Dynamic, Eigen::Dynamic> pow(int a, Eigen::Matrix<_T_, Eigen::Dynamic, Eigen::Dynamic> b)
  {
    return pow((double)a, b);
  }
  inline Eigen::MatrixXd pow_ew(Eigen::MatrixXd a, double b)
  {
    return a.array().pow(b);
  }
  inline Eigen::MatrixXd pow_ew(double a, Eigen::MatrixXd b)
  {
    Eigen::MatrixXd r(b.cols(), b.rows());
    for(int i = 0; i < b.cols(); ++i)
    {
      for(int j = 0; j < b.rows(); ++j)
      {
        r(i, j) = std::pow(a, b(i,j));
      }
    }
    return r;
  }
  inline Eigen::MatrixXd eye(double a)
  {
    return Eigen::MatrixXd::Identity(a, a);
  }
  inline Eigen::MatrixXd eye(double a, double b)
  {
    return Eigen::MatrixXd::Identity(a, b);
  }
  inline Eigen::MatrixXd zeros(double a)
  {
    return Eigen::MatrixXd::Zero(a, a);
  }
  inline Eigen::MatrixXd zeros(double a, double b)
  {
    return Eigen::MatrixXd::Zero(a, b);
  }
  template<typename _T_>
  inline _T_ diag(const _T_& v)
  {
    if(v.rows() == 1)
    {
      _T_ r = _T_::Zero(v.cols(), v.cols());
      for(int i = 0; i < v.cols(); ++i)
      {
        r(i, i) = v(0,i);
      }
      return r;
    } else if(v.cols() == 1)
    {
      _T_ r = _T_::Zero(v.rows(), v.rows());
      for(int i = 0; i < v.rows(); ++i)
      {
        r(i, i) = v(i,0);
      }
      return r;
    } else {
      std::abort();
      return _T_(); // stupid compilers
    }
  }
  inline double rand()
  {
    return ::rand() / double(RAND_MAX);
  }
  inline Eigen::MatrixXd rand(int a, int b)
  {
    Eigen::MatrixXd m(a, b);
    for(int i = 0; i < a; ++i)
    {
      for(int j = 0; j < b; ++j)
      {
        m(i,j) = rand();
      }
    }
    return m;
  }
  inline Eigen::MatrixXd rand(int a)
  {
    return rand(a,a);
  }
  inline Eigen::MatrixXd linspace(double a, double b, int step = 100)
  {
    double inc = (b - a) / (step - 1);
    Eigen::MatrixXd m(step, 1);
    for(int i = 0; i < step; ++i)
    {
      m(i,1) = a;
      a += inc;
    }
    return m;
  }
  template<typename Derived>
  inline int size(const Eigen::MatrixBase<Derived>& m, int c)
  {
    switch(c)
    {
      case 1:
        return m.rows();
      case 2:
        return m.cols();
      default:
        return 1;
    }
  }
  template<typename Derived>
  inline int size(const Eigen::MatrixBase<Derived>& m, double *cols)
  {
    *cols = m.cols();
    return m.rows();
  }
  template<typename Derived>
  inline Eigen::MatrixXd size(const Eigen::MatrixBase<Derived>& m)
  {
    Eigen::Vector2d v;
    v << m.rows(), m.cols();
    return v.transpose();
  }
  template<typename Derived>
  inline int length(const Eigen::MatrixBase<Derived>& m)
  {
    return std::max(m.rows(), m.cols());
  }
  template<typename Derived>
  inline typename Eigen::MatrixBase<Derived>::Scalar trace(const Eigen::MatrixBase<Derived>& m)
  {
    return m.trace();
  }
  template<typename Derived>
  inline typename Eigen::MatrixBase<Derived>::Scalar min(const Eigen::MatrixBase<Derived>& m)
  {
    return m.minCoeff();
  }
  template<typename Derived>
  inline typename Eigen::MatrixBase<Derived>::Scalar max(const Eigen::MatrixBase<Derived>& m)
  {
    return m.maxCoeff();
  }
  
  #define CAUCHY_MAKE_CWISE_BINARY_OP(NAME, FUNCTOR) \
    template<typename Derived>  \
    EIGEN_STRONG_INLINE const Eigen::CwiseBinaryOp<Eigen::internal::FUNCTOR<typename Derived::Scalar>, const Derived, const Derived> \
    NAME(const Eigen::MatrixBase<Derived>& a, const Eigen::MatrixBase<Derived>& b) \
    { \
      return Eigen::CwiseBinaryOp<Eigen::internal::FUNCTOR<typename Derived::Scalar>, const Derived, const Derived>(a.derived(), b.derived()); \
    }
    
  CAUCHY_MAKE_CWISE_BINARY_OP(min, scalar_min_op)
  CAUCHY_MAKE_CWISE_BINARY_OP(max, scalar_max_op)
#if 0
  #define EIGEN_MAKE_CWISE_BINARY_OP(METHOD,FUNCTOR) \
  template<typename OtherDerived> \
  EIGEN_STRONG_INLINE const CwiseBinaryOp<FUNCTOR<Scalar>, const Derived, const OtherDerived> \
  (METHOD)(const EIGEN_CURRENT_STORAGE_BASE_CLASS<OtherDerived> &other) const \
  { \
    return CwiseBinaryOp<FUNCTOR<Scalar>, const Derived, const OtherDerived>(derived(), other.derived()); \
  }

  
  #define CAUCHY_EIGEN_ARRAY_DECLARE_GLOBAL_UNARY(NAME,FUNCTOR) \
  template<typename Derived> \
  inline const Eigen::CwiseUnaryOp<Eigen::internal::FUNCTOR<typename Derived::Scalar>, const Derived> \
  NAME(const Eigen::MatrixBase<Derived>& x) { \
    return x.derived(); \
  }
  CAUCHY_EIGEN_ARRAY_DECLARE_GLOBAL_UNARY(sqrt, scalar_sqrt_op)
#endif
}

#endif
