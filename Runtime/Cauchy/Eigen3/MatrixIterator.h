/*
 *  Copyright (c) 2010 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; see the file COPYING.RUNTIME.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 *
 * The file COPYING.RUNTIME.EXCEPTION contains an exception that allow
 * to use this file with other open source license
 */

#ifndef _CAUCHY_EIGEN3_MATRIX_ITERATOR_H_
#define _CAUCHY_EIGEN3_MATRIX_ITERATOR_H_

namespace Cauchy {
  template<class _T_>
  class MatrixIterator {
    public:
      MatrixIterator(double &value, const _T_& m) : m_x(0), m_y(0), m_value(value), m_matrix(m)
      {
      }
      bool isFinished() const {
        return m_y < m_matrix.rows() and not (m_y == m_matrix.rows() - 1 and m_x >= m_matrix.cols());
      }
      void next() {
        ++m_x;
        if(m_x >= m_matrix.cols())
        {
          m_x = 0;
          ++m_y;
        }
        m_value = m_matrix(m_x, m_y);
      }
    private:
      int m_x, m_y;
      double& m_value;
      _T_ m_matrix;
  };
}

#endif
