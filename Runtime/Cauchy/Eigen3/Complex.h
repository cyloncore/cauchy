/*
 *  Copyright (c) 2010 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; see the file COPYING.RUNTIME.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 *
 * The file COPYING.RUNTIME.EXCEPTION contains an exception that allow
 * to use this file with other open source license
 */

#ifndef _CAUCHY_COMPLEX_H_
#define _CAUCHY_COMPLEX_H_

#include <complex>

namespace Cauchy {
  std::complex<double> i(0, 1);
  
  inline double real(const std::complex<double>& c)
  {
    return c.real();
  }
  inline double real(double c)
  {
    return c;
  }
  inline Eigen::MatrixXd real(const Eigen::MatrixXcd& c)
  {
    return c.real();
  }
  inline Eigen::MatrixXd real(const Eigen::MatrixXd& c)
  {
    return c;
  }
  inline double imag(const std::complex<double>& c)
  {
    return c.imag();
  }
  inline double imag(double c)
  {
    return c;
  }
  inline Eigen::MatrixXd imag(const Eigen::MatrixXcd& c)
  {
    return c.imag();
  }
  inline Eigen::MatrixXd imag(const Eigen::MatrixXd& c)
  {
    return c;
  }
}

#endif
