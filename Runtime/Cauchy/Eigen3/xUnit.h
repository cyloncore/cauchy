/*
 *  Copyright (c) 2011 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; see the file COPYING.RUNTIME.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 *
 * The file COPYING.RUNTIME.EXCEPTION contains an exception that allow
 * to use this file with other open source license
 */

#include <Cauchy/xUnit/Asserts.h>
#include <Cauchy/Eigen3/Float.h>
#include <Cauchy/Eigen3/Norm.h>

namespace Cauchy
{
  namespace xUnit
  {
    template<typename _T_, typename _T2_>
    void assertElementsAlmostEqual(const Eigen::DenseBase<_T_>& _x,
                                   const Eigen::DenseBase<_T2_>& _y,
                                   double t, const std::string& msg, int line)
    {
      Eigen::Matrix< typename _T_::Scalar, Eigen::Dynamic, Eigen::Dynamic> x = _x;
      Eigen::Matrix< typename _T2_::Scalar, Eigen::Dynamic, Eigen::Dynamic> y = _y;
      
      Cauchy::xUnit::assertTrue(x.rows() == y.rows(), msg, line);
      Cauchy::xUnit::assertTrue(x.cols() == y.cols(), msg, line);
      if(x.rows() != y.rows() || x.cols() != y.cols()) return;
      for(int i = 0; i < x.rows(); ++i)
      {
        for(int j = 0; j < x.cols(); ++j)
        {
          assertElementsAlmostEqual(x.derived().coeff(i,j), y.derived().coeff(i,j), t, msg, line);
        }
      }
    }
    template<typename _T_>
    void assertVectorsAlmostEqual(double x, double y, const std::string& msg, int line)
    {
      assertElementsAlmostEqual(x, y, Cauchy::eps, msg, line);
    }
    template<typename _T_>
    void assertVectorsAlmostEqual(const Eigen::Matrix<_T_, Eigen::Dynamic, Eigen::Dynamic>& x,
                                   const Eigen::Matrix<_T_, Eigen::Dynamic, Eigen::Dynamic>& y,
                                   double t, const std::string& msg, int line)
    {
      assertElementsAlmostEqual(norm(x), norm(y), t, msg, line);
    }
  }
}
