/*
 *  Copyright (c) 2010 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; see the file COPYING.RUNTIME.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 *
 * The file COPYING.RUNTIME.EXCEPTION contains an exception that allow
 * to use this file with other open source license
 */

#ifndef _CAUCHY_EIGEN3_RANGE_H_
#define _CAUCHY_EIGEN3_RANGE_H_

namespace Cauchy {
  class Range {
    public:
      Range(Number start, Number end, Number step = 1) : m_start(start), m_end(end), m_step(step)
      {}
      Number start() const { return m_start; }
      Number end() const { return m_end; }
      Number step() const { return m_step; }
      operator Matrix() const {
        int idx = 0;
        Matrix m(1, std::size_t((m_end - m_start) / m_step + 1));
        if(m_step > 0)
        {
          for(Number s = m_start; s <= m_end; s += m_step, ++idx)
          {
            m(0, idx) = s;
          }
        } else {
          for(Number s = m_start; s >= m_end; s += m_step, ++idx)
          {
            m(0, idx) = s;
          }
        }
        return m;
      }
      Range operator-(Number _val) const
      {
        return Range(m_start - _val, m_end - _val, m_step);
      }
      Range operator+(Number _val) const
      {
        return Range(m_start + _val, m_end + _val, m_step);
      }
    private:
      Number m_start, m_end, m_step;
  };
  std::ostream& operator<< (std::ostream& ostr, const Range& range)
  {
    ostr << range.start() << ":" << range.step() << ":" << range.end();
    return ostr;
  }
}

#endif
