/*
 *  Copyright (c) 2010 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; see the file COPYING.RUNTIME.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 *
 * The file COPYING.RUNTIME.EXCEPTION contains an exception that allow
 * to use this file with other open source license
 */

#ifndef _CAUCHY_FFT_H_
#define _CAUCHY_FFT_H_

#include <unsupported/Eigen/FFT>

namespace Cauchy {
  Eigen::MatrixXcd fft(const Eigen::MatrixXd& m)
  {
    Eigen::FFT<double> fft;
    Eigen::MatrixXcd r(m.rows(), m.cols());
    for(int i = 0; i < m.cols(); ++i)
    {
      Eigen::VectorXcd rv;
      fft.fwd(rv, Eigen::VectorXd(m.col(i)));
      r.col(i) = rv;
    }
    return r;
  }
  Eigen::MatrixXcd ifft(const Eigen::MatrixXcd& m)
  {
    Eigen::FFT<double> fft;
    Eigen::MatrixXcd r(m.rows(), m.cols());
    for(int i = 0; i < m.cols(); ++i)
    {
      Eigen::VectorXcd rv;
      fft.inv(rv, Eigen::VectorXcd(m.col(i)));
      r.col(i) = rv;
    }
    return r;
  }
}

#endif
