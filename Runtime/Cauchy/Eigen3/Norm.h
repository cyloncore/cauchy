/*
 *  Copyright (c) 2010 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; see the file COPYING.RUNTIME.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 *
 * The file COPYING.RUNTIME.EXCEPTION contains an exception that allow
 * to use this file with other open source license
 */

#ifndef _CAUCHY_NORM_H_
#define _CAUCHY_NORM_H_

#include <Eigen/SVD>
#include "Infinity.h"

namespace Cauchy {
  double norm(const Eigen::MatrixXd& m, double p = 2)
  {
    if (p == 2)
    {
      Eigen::JacobiSVD<Eigen::MatrixXd> svd(m);
      return svd.singularValues()(0,0);
    }
    else if (p == 1) {
      double max = 0.0;
      for(int i = 0; i < m.cols(); ++i)
      {
        double cn = m.col(i).lpNorm<1>();
        if(cn > max) max = cn;
      }
      return max;
    } else if ( p == Inf) {
      double max = 0.0;
      for(int i = 0; i < m.rows(); ++i)
      {
        double cn = m.row(i).lpNorm<1>();
        if(cn > max) max = cn;
      }
      return max;
    } else if (p > 1)
    {
      // TODO unimplemented (but also unimplemented in matlab)
    }
    // TODO assert on value p < 1
    return -1;
  }
}

#endif
