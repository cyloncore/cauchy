/*
 *  Copyright (c) 2010 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; see the file COPYING.RUNTIME.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 *
 * The file COPYING.RUNTIME.EXCEPTION contains an exception that allow
 * to use this file with other open source license
 */

#ifndef _CAUCHY_EIGEN3_OCTAVE_H_
#define _CAUCHY_EIGEN3_OCTAVE_H_

namespace Cauchy {
  const char* OCTAVE_VERSION = "0";
  int index( const std::string& str, const std::string& match)
  {
    return str.find(match);
  }
  std::string tolower(const std::string& str)
  {
    const char* c = str.c_str();
    std::string res;
    for(unsigned int i = 0; i < str.length(); ++i)
    {
      res += ::tolower(c[i]);
    }
    return res;
  }
  std::string toupper(const std::string& str)
  {
    const char* c = str.c_str();
    std::string res;
    for(unsigned int i = 0; i < str.length(); ++i)
    {
      res += ::toupper(c[i]);
    }
    return res;
  }
}

#endif
