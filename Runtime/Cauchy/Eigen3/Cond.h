/*
 *  Copyright (c) 2010 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; see the file COPYING.RUNTIME.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 *
 * The file COPYING.RUNTIME.EXCEPTION contains an exception that allow
 * to use this file with other open source license
 */

#ifndef _CAUCHY_COND_H_
#define _CAUCHY_COND_H_

#include <Eigen/SVD>
#include "Inv.h"
#include "Norm.h"

namespace Cauchy {
  double cond(const Eigen::MatrixXd& m, double v = 2)
  {
    if(v == 2)
    {
      if(m.rows() == 0 || m.cols() == 0)
      {
        return 0.0;
      }
      Eigen::JacobiSVD<Eigen::MatrixXd> svd(m);
      double sigma_1 = svd.singularValues()(0);
      double sigma_n = svd.singularValues()(svd.singularValues().size() - 1);
      return sigma_1 / sigma_n;
    } else {
      return norm(m, v) * norm (inv (m), v);
    }
  }
}

#endif
