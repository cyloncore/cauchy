/*
 *  Copyright (c) 2010 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; see the file COPYING.RUNTIME.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 *
 * The file COPYING.RUNTIME.EXCEPTION contains an exception that allow
 * to use this file with other open source license
 */

#ifndef _CAUCHY_LU_H_
#define _CAUCHY_LU_H_

#include <Eigen/LU>

namespace Cauchy {
  namespace Internal {
    void split_lu(const Eigen::MatrixXd& lu, Eigen::MatrixXd& l, Eigen::MatrixXd& u)
    {
      l = Eigen::MatrixXd::Identity(lu.rows(), lu.cols());
      u = Eigen::MatrixXd::Zero(lu.rows(), lu.cols());
      u.resize(lu.rows(), lu.cols());
      for(int i = 0; i < lu.rows(); ++i)
      {
        for(int j = 0; j < lu.cols(); ++j)
        {
          if(j < i)
          {
            l(i,j) = lu(i,j);
          } else {
            u(i,j) = lu(i,j);
          }
        }
      }
    }
  }
  Eigen::MatrixXd lu(const Eigen::MatrixXd& m)
  {
    return m.lu().matrixLU();
  }
  Eigen::MatrixXd lu(const Eigen::MatrixXd& m, Eigen::MatrixXd* u)
  {
    Eigen::PartialPivLU<Eigen::MatrixXd> plu = m.lu();
    Eigen::MatrixXd l;
    Internal::split_lu(plu.matrixLU(), l, *u);
    return plu.permutationP().inverse() * l;
  }
  Eigen::MatrixXd lu(const Eigen::MatrixXd& m, Eigen::MatrixXd* u, Eigen::MatrixXd* p)
  {
    Eigen::PartialPivLU<Eigen::MatrixXd> plu = m.lu();
    Eigen::MatrixXd l;
    Internal::split_lu(plu.matrixLU(), l, *u);
    *p = plu.permutationP();
    return l;
  }
  Eigen::MatrixXd lu(const Eigen::MatrixXd& m, Eigen::MatrixXd* u, Eigen::MatrixXd* p, Eigen::MatrixXd* q)
  {
    Eigen::FullPivLU<Eigen::MatrixXd> plu = m.fullPivLu();
    Eigen::MatrixXd l;
    Internal::split_lu(plu.matrixLU(), l, *u);
    *p = plu.permutationP();
    *q = plu.permutationQ();
    return l;
  }
}

#endif
