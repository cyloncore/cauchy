/*
 *  Copyright (c) 2010 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include <Eigen/Core>
#include "../Unknown.h"

class TestUnknown : public CauchyTest::Case {
  public:
    TestUnknown() : CauchyTest::Case("TestMakeMatrix") {}
    virtual void runTest()
    {
      Eigen::MatrixXd m = (Eigen::MatrixXd(2,3) << 1, 2, 3, 4, 5, 6).finished();
      Eigen::MatrixXd m2 = Cauchy::Unknown("") + m;
      CAUCHYTEST_CHECK_EQUAL(m, m2);
      m2 = m + Cauchy::Unknown("");
      int a = Cauchy::Unknown("") * 10;
      CAUCHYTEST_CHECK_EQUAL(a, 10);
    }
};

