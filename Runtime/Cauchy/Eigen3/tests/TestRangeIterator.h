/*
 *  Copyright (c) 2010 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "../RangeIterator.h"

class TestRangeIterator : public CauchyTest::Case {
  public:
    TestRangeIterator() : CauchyTest::Case("RangeIterator") {}
    virtual void runTest()
    {
      int count = 0;
      Number i;
      int total = 0;
      for( Cauchy::RangeIterator r(i, 0, 10); r.isFinished(); r.next())
      {
        ++count;
        i = i + 1;
        total += i;
      }
      CAUCHYTEST_CHECK_EQUAL(i, 11);
      CAUCHYTEST_CHECK_EQUAL(count, 11);
      CAUCHYTEST_CHECK_EQUAL(total, 66);

    }
};
