/*
 *  Copyright (c) 2010 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

// #include <complex>
// 
// #include <Eigen/Eigen>
// 
// #include "../MatrixOperators.h"
// #include "../ComplexOperators.h"
// 
// template<typename _T_, typename _T2_>
// void testoperator2(_T_ t, _T2_ t2)
// {
//   t*t2;
//   t2*t;
//   t/t2;
//   t2/t;
//   t+t2;
//   t2+t;
//   t-t2;
//   t2-t;
// }
// 
// template<typename _T_>
// void testoperator(_T_ t)
// {
//   testoperator2<_T_, std::complex<int> >(t, 1);
//   testoperator2<_T_, std::complex<float> >(t, 1.0f);
//   testoperator2<_T_, std::complex<double> >(t, 1.0);
//   testoperator2<_T_, Eigen::MatrixXi >(t, Eigen::MatrixXi(2,2));
// }

int main()
{
//   testoperator<int>(1);
//   testoperator<float>(1.0f);
//   testoperator<double>(1.0);

//   testoperator< std::complex<int> >(1);
//   testoperator< std::complex<float> >(1.0f);
//   testoperator< std::complex<double> >(1.0);
}
