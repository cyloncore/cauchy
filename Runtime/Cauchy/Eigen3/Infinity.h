/*
 *  Copyright (c) 2010 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; see the file COPYING.RUNTIME.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 *
 * The file COPYING.RUNTIME.EXCEPTION contains an exception that allow
 * to use this file with other open source license
 */

#ifndef _CAUCHY_INFINITY_H_
#define _CAUCHY_INFINITY_H_

#include "NumberTypes.h"

namespace Cauchy {

  class Infinity {
    public:
      operator float ()
      {
        union {float f; uint32 i;} u;
        u.i = 0x7f800000;
        return u.f;
      }
      operator double ()
      {
        union {double d; uint64 i;} u;
        u.i = UINT64_C(0x7ff0000000000000);
        return u.d;
      }
      bool operator==(Infinity )
      {
        return true;
      }
      bool operator==(float f )
      {
        return (float)(*this) == f;
      }
      bool operator==(double f )
      {
        return (double)(*this) == f;
      }
  };
  bool operator==(double d, Infinity i)
  {
    return i == d;
  }
  Infinity Inf;
}

#endif
