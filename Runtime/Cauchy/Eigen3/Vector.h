/*
 *  Copyright (c) 2015 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; see the file COPYING.RUNTIME.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 *
 * The file COPYING.RUNTIME.EXCEPTION contains an exception that allow
 * to use this file with other open source license
 */

#ifndef _CAUCHY_EIGEN3_VECTOR_H_
#define _CAUCHY_EIGEN3_VECTOR_H_

#include <Eigen/Dense>

#include "Range.h"

namespace Cauchy
{
  template<typename Derived, typename OtherDerived>
  inline Eigen::MatrixXd cross(const Eigen::MatrixBase<Derived>& _a, const Eigen::MatrixBase<OtherDerived>& _b, int dim)
  {
    Eigen::MatrixXd ret(_a.rows(), _a.cols());
    switch(dim)
    {
      default:
      case 1:
//         CAUCHY_ASSERT(_a.rows() == 3 and _b.rows() == 3);
        {
          for(int i = 0; i < _a.cols(); ++i)
          {
            ret.col(i) = _a.template block<3, 1>(0, i).cross(_b.template block<3, 1>(0, i));
          }
        }
        break;
      case 2:
        {
          for(int i = 0; i < _a.rows(); ++i)
          {
            ret.row(i) = _a.template block<1, 3>(i, 0).cross(_b.template block<1, 3>(i, 0));
          }
        }
        break;
    }
    return ret;
  }
  template<typename Derived, typename OtherDerived>
  inline Eigen::MatrixXd cross(const Eigen::MatrixBase<Derived>& _a, const Eigen::MatrixBase<OtherDerived>& _b)
  {
    if(_a.rows() == 3 and _b.rows() == 3)
    {
      return cross(_a, _b, 1);
    } else if(_a.cols() == 3 and _b.cols() == 3)
    {
      return cross(_a, _b, 2);
    } else if(_a.rows() == 3 and _b.cols() == 3)
    {
      return cross(_a, _b.transpose(), 1);
    } else if(_a.cols() == 3 and _b.rows() == 3)
    {
      return cross(_a, _b.transpose(), 2);
    } else {
      // CAUCHY_ABORT()
      std::abort();
    }
  }
  
  template<typename Derived>
  inline Eigen::Block<const Eigen::Matrix<double, -1, -1>, -1, -1, false> __segment(const Eigen::MatrixBase<Derived>& _a, const Range& _range)
  {
    //CAUCHY_ASSERT(_range.step() == 1);
    if(_a.rows() == 1)
    {
      return _a.block(0, _range.start() - 1, 1, _range.end() - _range.start() + 1);
    } else {
      //CAUCH_ASSERT(_a.cols() == 1)
      return _a.block(_range.start() - 1, 0, _range.end() - _range.start() + 1, 1);
    }
  }
  template<typename Derived>
  inline Eigen::Block<Eigen::Matrix<double, -1, -1>, -1, -1, false> __segment(Eigen::MatrixBase<Derived>& _a, const Range& _range)
  {
    //CAUCHY_ASSERT(_range.step() == 1);
    if(_a.rows() == 1)
    {
      return _a.block(0, _range.start() - 1, 1, _range.end() - _range.start() + 1);
    } else {
      //CAUCH_ASSERT(_a.cols() == 1)
      return _a.block(_range.start() - 1, 0, _range.end() - _range.start() + 1, 1);
    }
  }
  
}

#endif
