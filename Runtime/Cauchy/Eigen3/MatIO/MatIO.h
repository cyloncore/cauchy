#include <Eigen/Core>

namespace Cauchy
{
  class MatIO
  {
  public:
    enum Mode { READONLY, WRITEONLY };
    enum Compression { NOCOMPRESSION, ZLIBCOMPRESSION };
  public:
    MatIO();
    ~MatIO();
    bool open(const std::string& _filename, Mode _mode);
    void close();
    template<typename _T_>
    _T_ read(const std::string& _varname) const;
    template<typename _T_>
    bool write(const std::string& _varname, const _T_& _value, Compression _compression = NOCOMPRESSION) const;
  private:
    struct Private;
    Private* const d;
  };
}
