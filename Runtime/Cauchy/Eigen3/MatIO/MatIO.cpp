#include "MatIO.h"

#include <matio.h>

#include "../../Common/Debug.h"

using namespace Cauchy;

struct MatIO::Private
{
  Private() : matfp(nullptr) {}
  mat_t* matfp;
};

MatIO::MatIO() : d(new Private)
{

}

MatIO::~MatIO()
{
  close();
  delete d;
}

bool MatIO::open(const std::string& _filename, Mode _mode)
{
  if(d->matfp) return false;
  
  switch(_mode)
  {
    case READONLY:
      d->matfp = Mat_Open(_filename.c_str(), MAT_ACC_RDONLY);
      break;
    case WRITEONLY:
      d->matfp = Mat_CreateVer(_filename.c_str(), NULL, MAT_FT_DEFAULT);
      break;
  }
  return d->matfp;
}

void MatIO::close()
{
  if(d->matfp)
  {
    Mat_Close(d->matfp);
    d->matfp = nullptr;
  }
}

namespace Cauchy
{

  template <>
  Eigen::MatrixXd MatIO::read<Eigen::MatrixXd>(const std::string& _varname) const
  {
    matvar_t* matvar = Mat_VarRead(d->matfp, _varname.c_str());
    CAUCHY_ASSERT(matvar->data_type == MAT_T_DOUBLE);
    Eigen::MatrixXd r;
    switch(matvar->rank)
    {
      case 1:
      {
        r = Eigen::Map<Eigen::MatrixXd>((double*)matvar->data, matvar->dims[0], 1);
        break;
      }
      case 2:
      {
        // Matrix
        r = Eigen::Map<Eigen::MatrixXd>((double*)matvar->data, matvar->dims[0], matvar->dims[1]);
        break;
      }
    }
    Mat_VarFree(matvar);
    return r;
  }
  
  template<>
  bool MatIO::write<Eigen::MatrixXd>(const std::string& _varname, const Eigen::MatrixXd& _value, Compression _compression) const
  {
    std::size_t dims[2]; dims[0] = _value.rows(); dims[1] = _value.cols();
    matvar_t* matvar = Mat_VarCreate(_varname.c_str(), MAT_C_DOUBLE, MAT_T_DOUBLE, 2, dims, const_cast<double*>(_value.data()), 0);
    if(matvar)
    {
      switch(_compression)
      {
        default:
        case NOCOMPRESSION:
          Mat_VarWrite(d->matfp, matvar, MAT_COMPRESSION_NONE);
          break;
        case ZLIBCOMPRESSION:
          Mat_VarWrite(d->matfp, matvar, MAT_COMPRESSION_ZLIB);
          break;
      }
      Mat_VarFree(matvar);
      return true;
    } else {
      std::cerr << "Failed to create a variable for " << _varname << std::endl;
      return false;
    }
  }
}
