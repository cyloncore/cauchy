/*
 *  Copyright (c) 2010 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; see the file COPYING.RUNTIME.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 *
 * The file COPYING.RUNTIME.EXCEPTION contains an exception that allow
 * to use this file with other open source license
 */

#ifndef _CAUCHY_TRIGONOMETRY_H_
#define _CAUCHY_TRIGONOMETRY_H_

#include <cmath>

namespace Eigen
{
  namespace internal
  {
    template<typename Scalar> struct scalar_round_op
    {
      EIGEN_EMPTY_STRUCT_CTOR(scalar_round_op)
      typedef typename NumTraits<Scalar>::Real result_type;
      EIGEN_STRONG_INLINE const result_type operator() (const Scalar& a) const { using std::round; return round(a); }
    };
    template<typename Scalar>
    struct functor_traits<scalar_round_op<Scalar> >
    {
      enum {
        Cost = NumTraits<Scalar>::AddCost
      };
    };
    
  }
}

namespace Cauchy {
  inline double cos(double d)
  {
    return std::cos(d);
  }
  inline double sin(double d)
  {
    return std::sin(d);
  }
  inline double tan(double d)
  {
    return std::tan(d);
  }
  inline double atan(double d)
  {
    return std::atan(d);
  }
  inline double atan2(double a, double b)
  {
    return std::atan2(a, b);
  }
  inline double exp(double d)
  {
    return std::exp(d);
  }
  inline double log(double d)
  {
    return std::log(d);
  }
  inline double gamma(double d)
  {
    return gamma(d);
  }
  inline double sqrt(double d)
  {
    return std::sqrt(d);
  }
  inline double abs(double d)
  {
    return std::abs(d);
  }
  inline double round(double d)
  {
    return std::round(d);
  }
  
  
#define CAUCHY_EIGEN_ARRAY_DECLARE_GLOBAL_UNARY(NAME,FUNCTOR) \
  template<typename Derived> \
  inline const Eigen::CwiseUnaryOp<Eigen::internal::FUNCTOR<typename Derived::Scalar>, const Derived> \
  NAME(const Eigen::MatrixBase<Derived>& x) { \
    return x.derived(); \
  }
  CAUCHY_EIGEN_ARRAY_DECLARE_GLOBAL_UNARY(sqrt, scalar_sqrt_op)
  CAUCHY_EIGEN_ARRAY_DECLARE_GLOBAL_UNARY(abs, scalar_abs_op)
  CAUCHY_EIGEN_ARRAY_DECLARE_GLOBAL_UNARY(round, scalar_round_op)
}

#endif
