/*
 *  Copyright (c) 2010 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; see the file COPYING.RUNTIME.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 *
 * The file COPYING.RUNTIME.EXCEPTION contains an exception that allow
 * to use this file with other open source license
 */

#ifndef _CAUCHY_COMPLEX_OPERATORS_H_
#define _CAUCHY_COMPLEX_OPERATORS_H_

namespace std {
#define MAKE_COMPLEX_OPERATOR(_op_, _opname_, _t1_, _t2_)                                 \
  std::complex<_t1_> _opname_(const std::complex<_t1_>& a, const std::complex<_t2_>& b)   \
  {                                                                                       \
    return a _op_ std::complex<_t1_>(b.real(), b.imag());                                 \
  }                                                                                       \
  std::complex<_t1_> _opname_(const std::complex<_t2_> a, const std::complex<_t1_>& b)    \
  {                                                                                       \
    return std::complex<_t1_>(a.real(), b.imag()) _op_ b;                                 \
  }                                                                                       \
  std::complex<_t1_> _opname_(const std::complex<_t1_>& a, _t2_ b)                        \
  {                                                                                       \
    return a _op_ _t1_(b);                                                                \
  }                                                                                       \
  std::complex<_t1_> _opname_(_t2_ a, const std::complex<_t1_>& b)                        \
  {                                                                                       \
    return _t1_(a) _op_ b;                                                                \
  }                                                                                       \
  std::complex<_t1_> _opname_(const std::complex<_t2_>& a, _t1_ b)                        \
  {                                                                                       \
    return std::complex<_t1_>(a.real(), a.imag()) _op_ b;                                 \
  }                                                                                       \
  std::complex<_t1_> _opname_(_t1_ a, const std::complex<_t2_>& b)                        \
  {                                                                                       \
    return a _op_ std::complex<_t1_>(b.real(), b.imag());                                 \
  }                                                                                       \

#define MAKE_COMPLEX_OPERATORS(_t1_, _t2_)                                                \
  MAKE_COMPLEX_OPERATOR(*, operator*, _t1_, _t2_)                                         \
  MAKE_COMPLEX_OPERATOR(/, operator/, _t1_, _t2_)                                         \
  MAKE_COMPLEX_OPERATOR(+, operator+, _t1_, _t2_)                                         \
  MAKE_COMPLEX_OPERATOR(-, operator-, _t1_, _t2_)
  
  MAKE_COMPLEX_OPERATORS(float, int)
  MAKE_COMPLEX_OPERATORS(double, int)
  MAKE_COMPLEX_OPERATORS(double, float)
}

#endif
