/*
 *  Copyright (c) 2010,2015 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; see the file COPYING.RUNTIME.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 *
 * The file COPYING.RUNTIME.EXCEPTION contains an exception that allow
 * to use this file with other open source license
 */

#ifndef _CAUCHY_EIGEN3_RANGE_ITERATOR_H_
#define _CAUCHY_EIGEN3_RANGE_ITERATOR_H_

#include "Range.h"

namespace Cauchy {
  class RangeIterator {
    public:
      RangeIterator(Number &value, Number start, Number end, Number step = 1) : m_r(start, end, step), m_pos(m_r.start()), m_value(value)
      {
        m_value = m_pos;
      }
      RangeIterator(Number &value, const Range& r) : m_r(r), m_pos(m_r.start()), m_value(value)
      {
        m_value = m_pos;
      }
      ~RangeIterator()
      {
        m_value -= m_r.step();
      }
      bool isFinished() const {
        return (m_r.step() > 0 and m_pos <= m_r.end()) or (m_r.step() < 0 and m_pos >= m_r.end());
      }
      void next() {
        m_pos += m_r.step();
        m_value = m_pos;
      }
    private:
      Range m_r;
      Number m_pos;
      Number& m_value;
  };
}

#endif
