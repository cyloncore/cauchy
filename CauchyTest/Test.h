/*
 *  Copyright (c) 2008 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef _CAUCHYTEST_TEST_H_
#define _CAUCHYTEST_TEST_H_

#include <CauchyTest/Suite.h>
#include <CauchyTest/Result.h>

#include <iostream>

/**
 * @internal
 * @ingroup CauchyTest
 * Start generating the main function of the test program.
 * @param testname name of the test
 */
#define CAUCHYTEST_MAIN_BEGIN(testname) \
  int main(int /*argc*/, char** /*argv*/) \
  { \
    CauchyTest::Suite test(#testname); \

/**
 * @internal
 * @ingroup CauchyTest
 * Add a test \ref CauchyTest::Case to the main test \ref CauchyTest::Suite .
 * @param case name of the test
 */
#define CAUCHYTEST_MAIN_ADD_CASE(case) \
    test.addCase(new case());

/**
 * @internal
 * @ingroup CauchyTest
 * End of the main function.
 */
#define CAUCHYTEST_MAIN_END() \
    test.run(0); \
    test.result()->dump(); \
    return test.result()->countFailed(); \
  }

#endif
