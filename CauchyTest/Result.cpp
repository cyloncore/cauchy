/*
 *  Copyright (c) 2008 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "Result.h"

using namespace CauchyTest;

#include <list>
#include <iostream>

struct Result::Private
{
  int countFailed;
  int countPassed;
  Result* parent;
  std::string name;
  std::list<Result*> children;
};

Result::Result(const std::string& _name, Result* _parent ) : d(new Private)
{
  d->name = _name;
  d->parent = _parent;
  d->countFailed = 0;
  d->countPassed = 0;
  if( d->parent )
  {
    d->parent->d->children.push_back(this);
  }
}

Result::~Result()
{
  delete d;
}

const std::string& Result::name() const
{
  return d->name;
}

void Result::incPassed()
{
  ++d->countPassed;
  if( d->parent )
    d->parent->incPassed();
}

int Result::countPassed() const
{
  return d->countPassed;
}

void Result::incFailed()
{
  ++d->countFailed;
  if( d->parent )
    d->parent->incFailed();
}

int Result::countFailed() const
{
  return d->countFailed;
}

void Result::dump(const std::string& _prefix ) const
{
  std::cout << _prefix << d->name << ": Passed = " << countPassed() << " Failed = " << countFailed() << std::endl;
  std::string nprefix_ = _prefix + " ";
  for(std::list<Result*>::iterator it = d->children.begin(); it != d->children.end(); ++it)
  {
    (*it)->dump( nprefix_ );
  }
}
