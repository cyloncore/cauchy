/*
 *  Copyright (c) 2008 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "CauchyTest/Test.h"

class TestResult : public CauchyTest::Case {
  public:
    TestResult() : CauchyTest::Case("TestResult") {}
    virtual void runTest()
    {
      CauchyTest::Result r("Test");
      CauchyTest::Result r2("Test2", &r);
      CAUCHYTEST_CHECK_EQUAL(r.name(), "Test");
      CAUCHYTEST_CHECK_EQUAL(r.countPassed(), 0);
      CAUCHYTEST_CHECK_EQUAL(r.countFailed(), 0);
      CAUCHYTEST_CHECK_EQUAL(r2.name(), "Test2");
      r.incPassed();
      CAUCHYTEST_CHECK_EQUAL(r.countPassed(), 1);
      CAUCHYTEST_CHECK_EQUAL(r2.countPassed(), 0);
      r2.incPassed();
      CAUCHYTEST_CHECK_EQUAL(r.countPassed(), 2);
      CAUCHYTEST_CHECK_EQUAL(r2.countPassed(), 1);
      CAUCHYTEST_CHECK_EQUAL(r.countFailed(), 0);
      CAUCHYTEST_CHECK_EQUAL(r2.countFailed(), 0);
      r.incFailed();
      CAUCHYTEST_CHECK_EQUAL(r.countFailed(), 1);
      CAUCHYTEST_CHECK_EQUAL(r2.countFailed(), 0);
      r2.incFailed();
      CAUCHYTEST_CHECK_EQUAL(r.countFailed(), 2);
      CAUCHYTEST_CHECK_EQUAL(r2.countFailed(), 1);
      CAUCHYTEST_CHECK_EQUAL(r.countPassed(), 2);
      CAUCHYTEST_CHECK_EQUAL(r2.countPassed(), 1);
    }
};

CAUCHYTEST_MAIN_BEGIN(TestCauchyTest)
CAUCHYTEST_MAIN_ADD_CASE(TestResult)
#ifdef CAUCHYTEST_HAVE_THREAD
CAUCHYTEST_MAIN_ADD_CASE(TestThread)
#endif
CAUCHYTEST_MAIN_END()
