/*
 *  Copyright (c) 2008 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "Case.h"

#include "Result.h"

#include <iostream>

using namespace CauchyTest;

struct Case::Private
{
  std::string name;
  Result* result;
};

Case::Case(const std::string& _name) : d(new Private)
{
  d->name = _name;
  d->result = 0;
}

Case::~Case()
{
  delete d->result;
  delete d;
}

void Case::run(Result* _result)
{
  if(d->result)
  {
    CAUCHYTEST_CHECK_MESSAGE(false, "case has already been runed");
  } else {
    d->result = new Result( d->name, _result);
    runTest();
  }
}

const std::string& Case::name() const
{
  return d->name;
}

void Case::check(bool v, const std::string& message)
{
  if(v)
  {
    d->result->incPassed();
  } else {
    d->result->incFailed();
    std::cerr << message << std::endl;
  }
}

Result* Case::result() {
  return d->result;
}
