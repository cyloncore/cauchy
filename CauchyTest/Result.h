/*
 *  Copyright (c) 2008 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef _CAUCHYTEST_RESULT_H_
#define _CAUCHYTEST_RESULT_H_

#include <string>

namespace CauchyTest {
  
  /**
   * @internal
   * @ingroup CauchyTest
   * This class holds the result of a test Case.
   * 
   */
  class Result {
    public:
      /**
       * Construct a result object.
       * @param _name name of the test case associated with this result
       * @param _parent result object of the test suite
       */
      Result(const std::string& _name, Result* _parent = 0 );
      ~Result();
    public:
      /**
       * @return the name of the test case
       */
      const std::string& name() const;
      /**
       * Increment when a test has passed.
       * It will also increment the number of passed test of the parent.
       */
      void incPassed();
      /**
       * @return the number of successfull test
       */
      int countPassed() const;
      /**
       * Increment when a test has failed.
       * It will also increment the number of failed test of the parent.
       */
      void incFailed();
      /**
       * @return the number of failed test
       */
      int countFailed() const;
      /**
       * Dump on the standard output the number of successfull test and failed test.
       * @param prefix a string with indentation space
       */
      void dump(const std::string& prefix = "") const;
    private:
      struct Private;
      Private* const d;
  };
}

#endif
