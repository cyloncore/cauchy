#include "m2mml.h"

#include <Cauchy/Source.h>
#include <Cauchy/DeclarationsRegistry.h>
#include <Cauchy/Options.h>
#include <Cauchy/CompilationMessages.h>

bool m2mml(const std::string& _source, std::string& _output, std::string* _error_msg  )
{
  Cauchy::String source = _source;
  Cauchy::Source p;
  Cauchy::Options options;
  p.setSource( "m2mml", source, 0 );
  p.compile();
  if(not p.isCompiled())
  {
    if(_error_msg) *_error_msg = p.compilationMessages().toString();
    return false;
  }
  _output = p.generate(options, "MathML");
  return true;
}
