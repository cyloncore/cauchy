/*
 *  Copyright (c) 2008 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "CauchyTest/Test.h"
#include "m2mml.h"

class Testm2mml : public CauchyTest::Case {
  public:
    Testm2mml() : CauchyTest::Case("Testm2mml") {}
    virtual void runTest()
    {
      std::string source = "1+1";
      std::string result;
      
      CAUCHYTEST_CHECK(m2mml(source, result));
    }
};

CAUCHYTEST_MAIN_BEGIN(Testm2mml)
CAUCHYTEST_MAIN_ADD_CASE(Testm2mml)
CAUCHYTEST_MAIN_END()
