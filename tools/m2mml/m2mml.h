#include <string>

/** * @param source the string containing the matlab/octave source code
 * @return a string with the mathml document
 */
bool m2mml(const std::string& _source, std::string& _output, std::string* _error_msg = 0 );
