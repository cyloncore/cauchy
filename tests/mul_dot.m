A = [1  2   3;   5   7   9];
B = [4  6   8;   10   12   14];
C = A .* B;

assertElementsAlmostEqual( C, [     4    12    24;
    50    84   126 ])
