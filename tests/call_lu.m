A = [ 0.9619    0.8687    0.8001    0.2638;
      0.0046    0.0844    0.4314    0.1455;
      0.7749    0.3998    0.9106    0.1361;
      0.8173    0.2599    0.1818    0.8693 ];

L_r = [ 0.9619 0.8687 0.8001 0.2638;
    0.849673 -0.478211 -0.498023  0.645156;
    0.805593  0.627378  0.578494 -0.481172;
    0.0047822 -0.167804  0.594654  0.538629 ];

assertElementsAlmostEqual(lu(A), L_r, 1e-6 );

U_r = [    0.9619    0.8687    0.8001    0.2638;
        0 -0.478211 -0.498023  0.645156;
        0         0  0.578494 -0.481172;
        0         0         0  0.538629 ];

[L U] = lu(A);
assertElementsAlmostEqual(U, U_r, 1e-6 );
assertElementsAlmostEqual(A, L * U);

[L U P] = lu(A);
assertElementsAlmostEqual( L, [ 1         0         0         0;
            0.849673         1         0         0;
            0.805593  0.627378         1         0;
            0.0047822 -0.167804  0.594654         1],1e-6);

assertElementsAlmostEqual(U, U_r, 1e-6 );
assertElementsAlmostEqual(P, [ 1 0 0 0;
      0 0 0 1;
      0 0 1 0;
      0 1 0 0 ])
assertElementsAlmostEqual(A, inv(P) * L * U);
