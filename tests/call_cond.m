A = [ 0.9619    0.8687    0.8001    0.2638;
      0.0046    0.0844    0.4314    0.1455;
      0.7749    0.3998    0.9106    0.1361;
      0.8173    0.2599    0.1818    0.8693 ];

cond(A)

assertElementsAlmostEqual(cond(A), 1.04560895215412e+01, 1e-10);

assertElementsAlmostEqual(cond(A,1), 1.76564495143847e+01, 1e-10);

assertElementsAlmostEqual(cond(A,2), 1.04560895215412e+01, 1e-10);

assertElementsAlmostEqual(cond(A,Inf), 1.73299845833086e+01, 1e-10);
