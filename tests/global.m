function f1
 global gl_a;
 global gl_b;
 gl_a = 42 + gl_b;
end

function f2
 global gl_a;
 global gl_b;
 gl_b = 42 + gl_a;
end

global gl_a;
global gl_b;

gl_a = 2;
gl_b = 3;

f1

assertElementsAlmostEqual(gl_a, 45)
assertElementsAlmostEqual(gl_b, 3)

f2

assertElementsAlmostEqual(gl_a, 45)
assertElementsAlmostEqual(gl_b, 87)
