

include_directories(${CMAKE_SOURCE_DIR}/Runtime ${CMAKE_BINARY_DIR}/Runtime ${CMAKE_CURRENT_SOURCE_DIR})

macro(generate_m_test name)
  add_custom_command(
      DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/${name}.m cauchymc
      COMMAND ${CAUCHYMC}
      ARGS -I ${CMAKE_SOURCE_DIR}/Runtime -I ${CMAKE_CURRENT_SOURCE_DIR} -i tests.cfd -o ${CMAKE_CURRENT_BINARY_DIR}/${name}.cpp ${CMAKE_CURRENT_SOURCE_DIR}/${name}.m
      OUTPUT ${name}.cpp )
endmacro()

macro(build_m_test name)
  generate_m_test(${name})
  add_executable(test_${name} ${name}.cpp main.cpp)
endmacro()

macro(add_m_test name)
  generate_m_test(${name})
  add_executable(test_${name} ${name}.cpp)
  target_link_libraries(test_${name} CauchyXUnit)
  add_test(test_${name} test_${name})
endmacro()

add_m_test(access)
add_m_test(assign)
add_m_test(add)
add_m_test(add_dot)
add_m_test(add_integers)
add_m_test(comment)
add_m_test(complex)
add_m_test(creation_coma_simple)
add_m_test(creation_integers)
add_m_test(creation_space_simple)
add_m_test(creation_plusminus_matrix)
add_m_test(div_dot)
add_m_test(exponent)
add_m_test(for)
add_m_test(if)
add_m_test(ifelse)
add_m_test(ifelseif)
add_m_test(ifelseifelse)
add_m_test(linecomment)
add_m_test(minus)
add_m_test(mul)
add_m_test(mul_dot)
add_m_test(neg_mat)
add_m_test(overrideconstant)
add_m_test(overrideconstantloop)
add_m_test(unkwnow)
add_m_test(add_integer_to_mat)
add_m_test(range)
add_m_test(singlelineif)
add_m_test(setvalue)
add_m_test(expressions)
add_m_test(hello_world)
add_m_test(system)
add_m_test(multilines)
add_m_test(parenthesis)
add_m_test(transpose)
add_m_test(while)
add_m_test(whilebreak)

# Call
add_m_test(call_clc)
add_m_test(call_cond)
add_m_test(call_cross)
add_m_test(call_diag)
add_m_test(call_disp)
add_m_test(call_eig)
add_m_test(call_eye)
add_m_test(call_feval)
add_m_test(call_fft)
add_m_test(call_function_return_two)
add_m_test(call_function_two_arguments)
add_m_test(call_function_return_two_only_one)
add_m_test(call_lu)
add_m_test(call_minmax)
add_m_test(call_mldivide)
add_m_test(call_norm)
add_m_test(call_rand)
add_m_test(call_sort)
add_m_test(call_sqrt)
add_m_test(call_trace)
add_m_test(call_svd)
add_m_test(call_zeros)

# build_m_test(add_unknown)
add_m_test(global)
add_m_test(function_simple)
add_m_test(function_one_argument)
add_m_test(function_two_arguments)
add_m_test(function_return_one)
add_m_test(function_return_two)
add_m_test(function_persistent)
add_m_test(function_multiple_variables)
add_m_test(function_structure)
add_m_test(function_transpose)
# add_m_test(obench)
