A = [1  2   3;   5   7   9];
B = [4  6   8;   10   12   14];
C = 1 + 2 * ( B - A);
E = 3;
D= ( 1 - 3 * ( B + A) ) * ( E - 1 + 2);

assertElementsAlmostEqual( C, [ 7    9   11;
   11   11   11 ])

assertElementsAlmostEqual( D, [ -56   -92  -128;
  -176  -224  -272 ])
