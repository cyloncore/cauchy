function [A, B] = function_return_two
  A = [10 12 ; 24 8]
  B = [5; 2]
end

assertElementsAlmostEqual( function_return_two, [10 12 ; 24 8])

[C D ] = function_return_two()

assertElementsAlmostEqual( C, [10 12 ; 24 8])
assertElementsAlmostEqual( D, [5; 2])
