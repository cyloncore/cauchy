a = 1:10
b = 1:2:10
c = 5
d = c:-1:0
e = c-1:-1:0

assertElementsAlmostEqual( a, [ 1    2    3    4    5    6    7    8    9   10 ] )
assertElementsAlmostEqual( b, [ 1    3    5    7    9   ] )
assertElementsAlmostEqual( d, [ 5    4    3    2    1    0   ] )
assertElementsAlmostEqual( e, [ 4    3    2    1    0   ] )
