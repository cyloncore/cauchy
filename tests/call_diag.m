assertElementsAlmostEqual(diag([1 2 3]), [ 1 0 0; 0 2 0; 0 0 3 ]);
assertElementsAlmostEqual(diag([1i 2 3i]), [ 1i 0 0; 0 2 0; 0 0 3i ]);
assertElementsAlmostEqual(diag([1i; 2; 3i]), [ 1i 0 0; 0 2 0; 0 0 3i ]);
