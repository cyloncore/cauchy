function function_one_argument(B)
  A = [10 12 ; 24 8] + B;
  assertElementsAlmostEqual( A, [12 9; 29 7])
end

function_one_argument([2 -3; 5 -1])
