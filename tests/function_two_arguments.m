function function_two_arguments(B, C)
  A = [10 12 ; 24 8] + B * C
  assertElementsAlmostEqual( A, [18 16 ; 36 14])
end

function_two_arguments([2; 3], [4 2])
