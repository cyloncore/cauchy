a = 0;
if a<1000
  a += 1;
elseif a < 1500
  a -= 10;
else
  a = 5
end

assertElementsAlmostEqual( a, 1)

if a<0
  a += 1;
elseif a < 1500
  a -= 10;
else
  a = 5
end

assertElementsAlmostEqual( a, -9)

if a>10
  a += 1;
elseif a < -10
  a -= 10;
else
  a = 5
end

assertElementsAlmostEqual( a, 5)
