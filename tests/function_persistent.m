function B = function_persistent(init)
  persistent B

  if(init == 1)
    B = [1 2; 3 4]
  else
    B += B
  end
end

assertElementsAlmostEqual( function_persistent(1), [1 2; 3 4])
assertElementsAlmostEqual( function_persistent(0), [2 4; 6 8])
assertElementsAlmostEqual( function_persistent(0), [4 8; 12 16])

