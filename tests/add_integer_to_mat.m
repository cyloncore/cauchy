a = 1;
b = [2 3];
c = a + b;
assertElementsAlmostEqual(c, [ 3    4 ] );

d = b + a;
assertElementsAlmostEqual(d, [ 3    4 ] );

e = b + 2;
assertElementsAlmostEqual(e, [ 4    5 ] );

