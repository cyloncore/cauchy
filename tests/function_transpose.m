function function_transpose(A)
  assertElementsAlmostEqual( A, [10 24 ; 12 8])
end

B = [10 12 ; 24 8]

function_transpose(B')
