function A = function_return_one
  A = [10 12 ; 24 8];
end

B = function_return_one

assertElementsAlmostEqual( B, [10 12 ; 24 8])
