function a = ff(b)
 a = b - 1;
endfunction
assertElementsAlmostEqual(ff(2), 1);
assertElementsAlmostEqual(feval(@ff, 2), 1);