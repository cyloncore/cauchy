a = 1.0 + 2i;
M = [ 3i 1 ];

assertElementsAlmostEqual(real(a), 1);
assertElementsAlmostEqual(imag(a), 2);
assertElementsAlmostEqual(real(M), [0 1]);
assertElementsAlmostEqual(imag(M), [3 0]);