A = [1, 2, 3, 5, 7, 9];

assertElementsAlmostEqual(size(A), [1, 6])
assertElementsAlmostEqual(A(1,2), 2)

A = [1, 2, 3; 5, 7, 9];

assertElementsAlmostEqual(size(A), [2, 3])
assertElementsAlmostEqual(A(2,2), 7)
