A= [-1 2 3];
assertElementsAlmostEqual(A, [-1, 2, 3])
A= [ -1 -2 3];
assertElementsAlmostEqual(A, [-1, -2, 3])
A= [ -1 -2 +3];
assertElementsAlmostEqual(A, [-1, -2, 3])
A= [ -1+2 -2 3];
assertElementsAlmostEqual(A, [ 1, -2, 3])
