assertElementsAlmostEqual(sqrt(4), 2);
assertElementsAlmostEqual(sqrt([4 16; 9 25]), [2 4; 3 5]);