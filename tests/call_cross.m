a = [1 2 3];
b = [3 4 5];
assertElementsAlmostEqual(cross(a, b), [-2 4 -2]);
assertElementsAlmostEqual(cross(b, a), [ 2 -4 2]);
a = [ 1 2 3; 4 5 6; 7 8 9 ];
b = [ 3 4 5; 6 7 8; 9 0 1 ];

assertElementsAlmostEqual(cross(a, b), [-6  -56  -66; 12   32   42; -6   -6   -6]);
assertElementsAlmostEqual(cross(b, a), [ 6   56   66; -12  -32  -42; 6    6    6]);

assertElementsAlmostEqual(cross(a, b, 1), [-6  -56  -66; 12   32   42; -6   -6   -6]);
assertElementsAlmostEqual(cross(b, a, 1), [ 6   56   66; -12  -32  -42; 6    6    6]);

assertElementsAlmostEqual(cross(a, b, 2), [-2    4    -2;-2    4    -2;  8   74   -72]);
assertElementsAlmostEqual(cross(b, a, 2), [ 2   -4    2; 2   -4    2; -8  -74   72]);
