#include <Cauchy/Common/SharedData.h>
#include <Cauchy/Common/SharedPointerDeclaration.h>
#include <Cauchy/Eigen3.h>

struct TestStruct : public Cauchy::SharedData
{
  Cauchy::Matrix a, b;
  Number c;
};

CAUCHY_DECLARE_SHARED_POINTERS(TestStruct)
