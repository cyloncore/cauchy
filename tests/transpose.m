A = [1  2   3;   5   7   9];
B = [4  6   8;   10   12   14];
C = A' + B';
D = (A +B)';
E = [1  2   3   5   7   9]';

assertElementsAlmostEqual(C, [ 5   15;
    8   19;
   11   23 ])

assertElementsAlmostEqual(D, [ 5   15;
    8   19;
   11   23 ])

assertElementsAlmostEqual(E, [1 ; 2 ;  3 ;  5 ;  7 ;  9])
