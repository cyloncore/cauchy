M = eye(3);
assertElementsAlmostEqual(M, [ 1 0 0; 0 1 0; 0 0 1]);

M = eye(3,2);
assertElementsAlmostEqual(M, [ 1 0; 0 1; 0 0]);
