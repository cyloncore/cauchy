M = zeros(3);
assertElementsAlmostEqual(M, [ 0 0 0; 0 0 0; 0 0 0]);

M = zeros(3,2);
assertElementsAlmostEqual(M, [ 0 0; 0 0; 0 0]);
