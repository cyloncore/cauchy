a = 0;
if a<1000
  a += 1;
elseif a < 1500
  a -= 10;
end

assertElementsAlmostEqual( a, 1)

if a<0
  a += 1;
elseif a > 0
  a -= 10;
end

assertElementsAlmostEqual( a, -9)