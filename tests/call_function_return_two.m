function [A, B] = function_return_two
  A = [10 12 ; 24 8];
  B = [5; 2];
end

assertElementsAlmostEqual(function_return_two(), [10 12 ; 24 8]);

A = function_return_two();
assertElementsAlmostEqual(A, [10 12 ; 24 8]);

[A B] = function_return_two();
assertElementsAlmostEqual(A, [10 12 ; 24 8]);
assertElementsAlmostEqual(B, [5; 2]);