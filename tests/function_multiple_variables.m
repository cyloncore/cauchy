function c = function_multiple_variables()
  A = zeros(2, 2);
  B = eye(2, 2);
  c = 2;
  d = 4;
  e = 2;
  c = (A(1,1)+B(1,1)+c+d)*e;
end


assertElementsAlmostEqual(function_multiple_variables(), 14)