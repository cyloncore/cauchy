/*
 *  Copyright (c) 2008,2010 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include<list>

namespace Cauchy {
  class Variable;
  class String;
  class DeclarationsRegistry;
  class VariablesManager {
    public:
      VariablesManager(DeclarationsRegistry* registry);
      ~VariablesManager();
      void startContext();
      void endContext();
    public:
      std::list<Variable*> globales() const;
      Variable* declareGlobal(const String& name);
      Variable* getVariable(const String& name, bool isArgument);
      Variable* overrideConstant(const String& name);
      bool hasVariable(const String& name);
    private:
      struct Context;
      struct Private;
      Private* const d;
  };
}


