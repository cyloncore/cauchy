/*
 *  Copyright (c) 2008,2010 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "Token.h"

#include "../Debug.h"

using namespace Cauchy;
using namespace Cauchy::Cfd;

String Cfd::Token::typeToString(Type type)
{
  switch(type)
  {
    // Not really token
    case COMMENT:
      return "comment";
    case END_OF_FILE:
      return "end of file";
    case END_OF_LINE:
      return "end of line";
    case UNKNOWN:
      return "unknown token";
    case UNFINISHED_STRING:
      return "unfinished string";
    case IDENTIFIER:
      return "identifier";
    // Keywords,
    case CONSTANT:
      return "constant";
    case FUNCTION:
      return "function";
    case GLOBAL:
      return "global";
    case IN:
      return "in";
    case IS:
      return "is";
    case NAMED:
      return "named";
    case RETURNS:
      return "returns";
    case TAKES:
      return "takes";
    case STRUCTURE:
      return "structure";
    case CONTAINS:
      return "contains";
    case AS:
      return "as";
    case AND:
      return "and";
    // Types
    case COMPLEX:
      return "Complex";
    case MATRIX:
      return "Matrix";
    case CMATRIX:
      return "CMatrix";
    case NUMBER:
      return "Number";
    case STRING:
      return "String";
    case VARIANT:
      return "Variant";
    case VOID:
      return "Void";
    case FUNCTION_HANDLE:
      return "FunctionHandle";
  }
  return "[TODO] " + String::number( int(type) );
}

Cfd::Token::Token() : type(UNKNOWN), line(-1), column(-1)
{
}

Cfd::Token::Token(Type _type, int _line, int _column) : type(_type), line(_line), column(_column)
{
}

Cfd::Token::Token(Type _type, const String& _string, int _line, int _column) : type(_type), line(_line), column(_column), string(_string)
{
  CAUCHY_ASSERT( _type == IDENTIFIER or _type == COMMENT );
}
