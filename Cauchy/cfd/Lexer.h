/*
 *  Copyright (c) 2008,2010 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef _CAUCHY_CFD_LEXER_H_
#define _CAUCHY_CFD_LEXER_H_

#include <sstream>
#include <Cauchy/String.h>

namespace Cauchy {
  namespace Cfd {
    class Token;
    class Lexer {
      public:
        Lexer(std::istream* sstream);
        ~Lexer();
      public:
        Token nextToken();
      protected:
        /**
        * @return the next char and increment the column counter.
        */
        int getNextChar();
        /**
        * @return the next char that is not a separator (space, tab, return...)
        */
        int getNextNonSeparatorChar();
        /**
        * Cancel the reading of the previous char.
        */
        void unget();
        bool eof() const;
        int line() const;
        int column() const;
        /**
        * Get an identifier (or keyword) in the current flow of character.
        */
        Cauchy::String getIdentifier(int lastChar);
        Token getDigit(int lastChar);
        Token getString(int lastChar);
      private:
        struct Private;
        Private* const d;
    };
  }
}

#endif
