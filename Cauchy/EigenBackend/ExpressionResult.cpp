/*
 *  Copyright (c) 2010 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "ExpressionResult.h"

using namespace EigenBackend;

ExpressionResult::ExpressionResult(const Cauchy::String& result, const Cauchy::Type* type) : m_result(result), m_type(type)
{
}

ExpressionResult::ExpressionResult(const Cauchy::String& _start, const Cauchy::String& _step, const Cauchy::String& _end) : m_start(_start), m_step(_step), m_end(_end), m_type(Cauchy::Type::Range)
{
  m_result = "Cauchy::Range(" + m_start + ", " + m_end + ", " + m_step + ")";
}

ExpressionResult::~ExpressionResult()
{
}

Cauchy::String ExpressionResult::result() const
{
  return m_result;
}

Cauchy::String ExpressionResult::start() const
{
  return m_start;
}

Cauchy::String ExpressionResult::step() const
{
  return m_step;
}

Cauchy::String ExpressionResult::end() const
{
  return m_end;
}

const Cauchy::Type* ExpressionResult::type() const
{
  return m_type;
}
