/*
 *  Copyright (c) 2010 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "DeclarationsGenerator.h"

#include "../DeclarationsRegistry.h"
#include "../FunctionDeclaration.h"
#include "../Macros_p.h"
#include "../VariableDeclaration.h"

#include "GeneratorPrivateBase.h"

using namespace EigenBackend;

struct DeclarationsGenerator::Private : public GeneratorVisitorPrivate {
};

DeclarationsGenerator::DeclarationsGenerator() : d(new Private)
{
}

DeclarationsGenerator::~DeclarationsGenerator()
{
  delete d;
}

Cauchy::String DeclarationsGenerator::generate(const Cauchy::DeclarationsRegistry* _declarationsRegistry, const Cauchy::Options& /*options*/ )
{
  Cauchy::String body;
  body += "// Globals\n";
  std::list<const Cauchy::VariableDeclaration*> globals = _declarationsRegistry->globals();
  foreach( const Cauchy::VariableDeclaration* dcl, globals)
  {
    body += "extern " + d->typeName(dcl->type()) + " " + dcl->name() + ";\n";
  }
  body += "// Functions\n";
  std::list<const Cauchy::FunctionDeclaration*> functions = _declarationsRegistry->functions();
  foreach( const Cauchy::FunctionDeclaration* dcl, functions)
  {
    body += d->functionDeclaration(dcl, 0) + ";\n";
  }
  return d->header + body;
}
