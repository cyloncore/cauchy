/*
 *  Copyright (c) 2008,2010 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "VariablesManager.h"

#include <list>
#include <map>

#include "Debug.h"
#include "Variable.h"
#include "DeclarationsRegistry.h"

using namespace Cauchy;

struct VariablesManager::Context
{
  std::map< String, Variable* > variables;
};

struct VariablesManager::Private {
  DeclarationsRegistry* registry;
  std::list< Context > contextes;
  std::map< String, Variable* > globales;
  std::map< String, Variable* > currentGlobales;
  std::map< String, Variable* > constants;
  Variable* getVariableInMap( const std::map< String, Variable* >&, const String& );
};

Variable* VariablesManager::Private::getVariableInMap( const std::map< String, Variable* >& map, const String& n)
{
  for( std::map<String, Variable*>::const_iterator it = map.begin();
        it != map.end(); ++it)
  {
    if( it->first == n )
    {
      return it->second;
    }
  }
  return 0;
}

VariablesManager::VariablesManager(DeclarationsRegistry* registry) : d(new Private)
{
  d->registry = registry;
}

VariablesManager::~VariablesManager()
{
  delete d;
}

Variable* VariablesManager::declareGlobal(const String& name)
{
  if(d->globales.find(name) == d->globales.end())
  {
    const VariableDeclaration* declaration = 0;
    if(d->registry and( declaration = d->registry->global(name)))
    {
      d->globales[name] = new Variable(declaration);
    } else {
      return 0;
    }
  }
  Variable* var = d->globales[name];
  d->currentGlobales[name] = var;
  return var;
}

Variable* VariablesManager::getVariable(const String& name, bool isArgument)
{
  CAUCHY_DEBUG("getVariable " << name);
  for( std::list<Context>::const_iterator cit = d->contextes.begin();
       cit != d->contextes.end(); cit++)
  {
    Variable* var = d->getVariableInMap( cit->variables, name );
    if(var)
    {
      return var;
    }
  }
  // Maybe it is global ?
  Variable* var = d->getVariableInMap( d->currentGlobales, name);
  if(var) {
    return var;
  }
  // Maybe it is a constant
  var = d->getVariableInMap( d->constants, name);
  if(var) {
    return var;
  } else {
    // Maybe it is a constant that is not yet in the map
    const VariableDeclaration* declaration = d->registry->constant(name);
    if(declaration)
    {
      var = new Variable(declaration);
      d->constants[name] = var;
      return var;
    }
  }
  // Maybe it is not global, so create it locally
  var = new Variable(name, isArgument ? Variable::Argument : Variable::Normal);
  d->contextes.begin()->variables[name] = var;
  return var;
}

Variable* VariablesManager::overrideConstant(const String& name)
{
  Variable* var = new Variable(name, Variable::Normal);
  d->contextes.begin()->variables[name] = var;
  return var;
}

bool VariablesManager::hasVariable(const String& name)
{
  for( std::list<Context>::const_iterator cit = d->contextes.begin();
       cit != d->contextes.end(); cit++)
  {
    if(d->getVariableInMap( cit->variables, name ))
    {
      return true;
    }
  }
  // Maybe it is global ?
  if(d->getVariableInMap( d->currentGlobales, name)) {
    return true;
  }
  // Maybe it is a constant
  if(d->getVariableInMap( d->constants, name)) {
    return true;
  } else if(d->registry) {
    // Maybe it is a constant that is not yet in the map
    return d->registry->constant(name);
  }
  return false;
}

void VariablesManager::startContext()
{
  d->contextes.push_front( Context() );
}

void VariablesManager::endContext()
{
  d->contextes.pop_front();
  d->currentGlobales.clear();
}
