/*
 *  Copyright (c) 2010 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "Options.h"

#include <map>

#include "Debug.h"
#include "SharedPointer.h"
#include "String.h"

using namespace Cauchy;

struct Options::Private : public SharedPointerData {
  Private() {}
  Private(const Private& _rhs) : SharedPointerData(), options(_rhs.options)
  {}
  std::map<String, String> options;
};

Options::Options() : d(new Private)
{
  d->ref();
}

CAUCHY_SHARED_DATA(Options)

void Options::setOption(const String& name, const String& value)
{
  deref();
  d->options[name] = value;
}

bool Options::hasOption(const String& name) const
{
  return d->options.find(name) != d->options.end();
}

String Options::option(const String& name) const
{
  return d->options[name];
}
