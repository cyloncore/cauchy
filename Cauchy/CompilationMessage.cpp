/*
 *  Copyright (c) 2008-2009 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "CompilationMessage.h"

#include "Debug.h"
#include "SharedPointer.h"

using namespace Cauchy;

struct CompilationMessage::Private : public SharedPointerData {
  Private() {}
  Private(const Private& _rhs) : SharedPointerData(), errorMessage(_rhs.errorMessage), line(_rhs.line), fileName(_rhs.fileName), type(_rhs.type)
  {}
  String errorMessage;
  int line;
  String fileName;
  MessageType type;
};

CompilationMessage::CompilationMessage( MessageType type, const String& errorMessage, int line, const String& fileName ) : d(new Private)
{
  d->ref();
  d->errorMessage = errorMessage;
  d->line = line;
  d->fileName = fileName;
  d->type = type;
}

CAUCHY_SHARED_DATA(CompilationMessage)

int CompilationMessage::line() const
{
  return d->line;
}

String CompilationMessage::message() const
{
  return d->errorMessage;
}

String CompilationMessage::fileName() const
{
  return d->fileName;
}

void CompilationMessage::setLine(int line)
{
  deref();
  CAUCHY_ASSERT(d->line == -1);
  d->line = line;
}

void CompilationMessage::setFileName(const String& fileName)
{
  deref();
  CAUCHY_ASSERT(d->fileName == "");
  d->fileName = fileName;
}

CompilationMessage::MessageType CompilationMessage::type() const
{
  return d->type;
}
