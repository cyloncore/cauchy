/*
 *  Copyright (c) 2010 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "Version.h"


namespace Cauchy {
  String LibraryShortName() { return "Cauchy"; }
  String LibraryName() { return "Cauchy Mathematic Compiler"; }
  String LibraryCopyright() { return "Copyright (c) 2007-2010 Cyrille Berger (cberger@cberger.net)"; }
  String LibraryLicence() { return "GNU Lesser General Public License Version 2, or later"; }
  String LibraryVersionString() { return String::number( CAUCHY_VERSION_MAJOR ) + "." + String::number( CAUCHY_VERSION_MINOR ) + "." + String::number( CAUCHY_VERSION_REVISION ); }
  int LibraryVersionMajor()
  {
      return CAUCHY_VERSION_MAJOR;
  }
  int LibraryVersionMinor()
  {
      return CAUCHY_VERSION_MINOR;
  }
  int LibraryVersionRevision()
  {
      return CAUCHY_VERSION_REVISION;
  }
}

