/*
 *  Copyright (c) 2008-2009 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef _CAUCHY_ERROR_MESSAGE_H_
#define _CAUCHY_ERROR_MESSAGE_H_

#include <Cauchy/String.h>

class TestErrorMessage;

namespace Cauchy {
  /**
   * This class contains the information about a compilation message, such as the text
   * of the message, the line of occurance, and the name of the file.
   * 
   * @ingroup Cauchy
   */
  class CompilationMessage {
      friend class CompilerBase;
      friend class ::TestErrorMessage;
    public:
      enum MessageType {
        ERROR,
        WARNING
      };
    public:
      /**
       * Construct a new \ref CompilationMessage
       */
      CompilationMessage( MessageType _type, const String& errorMessage, int line = -1, const String& fileName = "");
      CompilationMessage( const CompilationMessage& );
      CompilationMessage& operator=(const CompilationMessage& rhs);
    public:
      ~CompilationMessage();
      /**
       * @return the line where the message occurs
       */
      int line() const;
      /**
       * @return the text of the message
       */
      String message() const;
      /**
       * @return the file name
       */
      String fileName() const;
      /**
       * @return the type of message
       */
      MessageType type() const;
    private:
      /**
       * Set the line number
       */
      void setLine(int line);
      /**
       * Set the file name
       */
      void setFileName(const String& fileName);
    private:
      void deref();
      struct Private;
      Private* d;
  };
  
}

#endif
