/*
 *  Copyright (c) 2010 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include <Cauchy/String.h>

namespace Cauchy {
  class CompilationMessages;
  class DeclarationsRegistry;
  class Options;
  class Source {
    public:
      Source();
      void setSource(const Cauchy::String& _fileName, const Cauchy::String& _source, DeclarationsRegistry* _registry);
      void compile();
      bool isCompiled();
      /**
       * @return a string with the content of the compilation error.
       */
      CompilationMessages compilationMessages() const;
      String generate(const Cauchy::Options& options, const Cauchy::String& backend = "eigen3");
    private:
      struct Private;
      Private* const d;
  };
}

