/*
 *  Copyright (c) 2008 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef _CAUCHY_UTILS_P_H_
#define _CAUCHY_UTILS_P_H_

#include "StdTypes.h"

namespace Cauchy {

  /**
   * @internal
   * Delete all pointers of a list or a vector.
   * @ingroup Cauchy
   */
  template<class _Type_>
  inline void deleteAll( _Type_& list )
  {
    for( typename _Type_::iterator it = list.begin(); it != list.end(); ++it)
    {
      delete *it;
    }
  }
  /**
   * @internal
   * Delete all second members of a map
   * @ingroup Cauchy
   */
  template<class _Type_>
  inline void deleteAllSecondMember( _Type_& map )
  {
    for( typename _Type_::iterator it = map.begin(); it != map.end(); ++it)
    {
      delete it->second;
    }
  }
  
  template<typename _T_>
  inline _T_ min( _T_ t1, _T_ t2 )
  {
    return t1 < t2 ? t1 : t2;
  }
  
  template<typename _T_>
  inline _T_ max( _T_ t1, _T_ t2 )
  {
    return t1 > t2 ? t1 : t2;
  }
  
  template<typename _T_>
  inline _T_ bound( _T_ min, _T_ val, _T_ max )
  {
    if( val < min ) return min;
    if( val > max ) return max;
    return val;
  }
  
}

#endif
