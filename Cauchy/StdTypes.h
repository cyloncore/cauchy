/*
 *  Copyright (c) 2009 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

//
// There is a conflict on how the stdint.h types are defined on the windows platform
// this raise the need for the cauchy_* counterpart
//

#ifndef _MSC_VER

#include <stdint.h>

typedef int8_t cauchy_int8;
typedef int16_t cauchy_int16;
typedef int32_t cauchy_int32;
typedef int64_t cauchy_int64;
typedef uint8_t cauchy_uint8;
typedef uint16_t cauchy_uint16;
typedef uint32_t cauchy_uint32;
typedef uint64_t cauchy_uint64;

#else // _MSC_VER

// include everywhere
#include <sys/types.h>

/* Exact-width integer types */
typedef signed char cauchy_int8;
typedef signed short cauchy_int16;
typedef signed long cauchy_int32;
typedef signed long long cauchy_int64;
typedef unsigned char cauchy_uint8;
typedef unsigned short cauchy_uint16;
typedef unsigned long cauchy_uint32;
typedef unsigned long long cauchy_uint64;

/* Limits of exact-width integer types */

#define INT8_MIN (-128)
#define INT16_MIN (-32768)
#define INT32_MIN (-2147483647 - 1)
#define INT64_MIN (-9223372036854775807LL - 1LL)

#define INT8_MAX (127)
#define INT16_MAX (32767)
#define INT32_MAX (2147483647)
#define INT64_MAX (9223372036854775807LL)

#define UINT8_MAX (255)
#define UINT16_MAX (65535)
#define UINT32_MAX (4294967295UL)
#define UINT64_MAX (18446744073709551615ULL)

/* Macros for minimum-width integer constant expressions */

#define INT8_C(x) x
#define INT16_C(x) x
#define INT32_C(x) x ## L
#define INT64_C(x) x ## LL

#define UINT8_C(x) x ## U
#define UINT16_C(x) x ## U
#define UINT32_C(x) x ## UL
#define UINT64_C(x) x ## ULL

/* Macros for greatest-width integer constant expressions */

#define INTMAX_C(x) x ## L
#define UINTMAX_C(x) x ## UL

#endif
