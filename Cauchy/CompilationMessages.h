/*
 *  Copyright (c) 2009 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef _CAUCHY_COMPILATION_MESSAGES_H_
#define _CAUCHY_COMPILATION_MESSAGES_H_

#include <list>


namespace Cauchy {
  class CompilationMessage;
  class String;
  /**
   * @ingroup Cauchy
   * This class countains the different compilation messages, errors and warnings.
   */
  class CompilationMessages {
    public:
      CompilationMessages();
      CompilationMessages& operator=(const CompilationMessages& );
      CompilationMessages(const CompilationMessages& );
      ~CompilationMessages();
      /**
       * @return the list of errors messages
       */
      const std::list<CompilationMessage>& errors() const;
      /**
       * @return the list of warnings messages
       */
      const std::list<CompilationMessage>& warnings() const;
      /**
       * @return the list of all messages
       */
      const std::list<CompilationMessage>& messages() const;
      /**
       * @return a string with all the messages, suitable for display
       */
      String toString() const;
    public:
      struct Private;
      Private* const d;
  };
}

#endif
