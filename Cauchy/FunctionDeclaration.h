/*
 *  Copyright (c) 2010 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef _CAUCHY_FUCTION_DEFINITION_H_
#define _CAUCHY_FUCTION_DEFINITION_H_

#include <vector>
#include <Cauchy/Type.h>

namespace Cauchy {
  class String;
  /**
   * This class contains a declaration of a function defined in a CFD.
   * @ingroup Cauchy
   */
  class FunctionDeclaration {
    friend class DeclarationsRegistry;
      FunctionDeclaration();
      ~FunctionDeclaration();
    public:
      /**
       * @return the list of returns types
       */
      const std::vector<const Type*>& returns() const;
      /**
       * @return the list of arguments types
       */
      const std::vector<const Type*>& arguments() const;
      /**
       * @return the name of the header containing that function
       */
      String header() const;
      /**
       * @return the name to use to call the function, or the function name if no call name was defined
       */
      String callName() const;
      /**
       * @return the name of the function
       */
      String name() const;
    private:
      struct Private;
      Private* const d;
  };
}

#endif
