/*
 *  Copyright (c) 2010 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "Type.h"

#include "String.h"
#include "VariableDeclaration.h"

namespace Cauchy {
  class VariableDeclaration;
  /**
   * @internal
   * @ingroup Cauchy
   *
   * This class represent a variable.
   */
  class Variable {
    public:
      enum Qualifier {
        Normal,
        Global, ///< global variable
        Argument, ///< this variable comes from a function argument
        Constant, ///< this is a global constant
        Persistent
      };
    public:
      Variable(const Cauchy::String& name, Qualifier qualifier);
      /**
       * Declare a global or constant variable.
       */
      Variable(const VariableDeclaration* declaration);
      /**
       * @return the name of the variable
       */
      Cauchy::String name() const;
      /**
       * @return the data type of the variable
       */
      const Type* type() const;
      /**
       * @return change the type of a variable
       *
       * This function does nothing in case a variable has a non null VariableDeclaration.
       * If the type is TUnknown, the type is changed. If the new type is different from
       * the previous one, it asserts.
       */
      bool setType(const Type* type);
      /**
       * @return the qualifier of the variable
       */
      Qualifier qualifier() const;
      /**
       * @return the declaration or null if the variable has no declaration
       */
      const VariableDeclaration* declaration() const;
      bool setPersistent();
    private:
      Cauchy::String m_name;
      const Type* m_type;
      Qualifier m_qualifier;
      const VariableDeclaration* m_declaration;
  };
}

