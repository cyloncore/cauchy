/*
 *  Copyright (c) 2010 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "FunctionDeclaration.h"

#include "FunctionDeclaration_p.h"

#include "Debug.h"

using namespace Cauchy;

FunctionDeclaration::FunctionDeclaration() : d(new Private)
{
}

FunctionDeclaration::~FunctionDeclaration()
{
  delete d;
}

const std::vector<const Type*>& FunctionDeclaration::returns() const
{
  return d->returns;
}

const std::vector<const Type*>& FunctionDeclaration::arguments() const
{
  return d->arguments;
}

String FunctionDeclaration::header() const
{
  return d->header;
}

String FunctionDeclaration::callName() const
{
  if(d->callName.isEmpty()) return d->name;
  return d->callName;
}

String FunctionDeclaration::name() const
{
  return d->name;
}
