/*
 *  Copyright (c) 2009 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "SharedPointer.h"

#include "Atomic.h"

#include "Debug.h"

using namespace Cauchy;

SharedPointerData::SharedPointerData(const SharedPointerData&)
{
  CAUCHY_ABORT("");
}

const SharedPointerData& SharedPointerData::operator=(const SharedPointerData&)
{
  CAUCHY_ABORT("");
  return *this;
}

SharedPointerData::SharedPointerData() : m_count(0)
{
}

SharedPointerData::~SharedPointerData()
{
}

void SharedPointerData::ref()
{
  llvm::sys::AtomicIncrement(&m_count);
}

int SharedPointerData::deref()
{
  llvm::sys::AtomicDecrement(&m_count);
  return m_count;
}

int SharedPointerData::count() const
{
  return m_count;
}
