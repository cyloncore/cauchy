/*
 *  Copyright (c) 2009 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef _AST_ANNOTATION_H_
#define _AST_ANNOTATION_H_

#include <Cauchy/String.h>

namespace Cauchy {
  namespace AST {
    /**
     * @internal
     * @ingroup Cauchy_AST
     *
     * This class countains information about a node in the AST, like the filename
     * and line number.
     */
    class Annotation {
      public:
        /// Creates an empty anotation
        Annotation() : m_fileName(""), m_line(-1) {}
        /// Creates a new anotation from a @p fileName and @p line
        Annotation(const String& fileName, int line) : m_fileName(fileName), m_line(line) {
        }
        ~Annotation();
        /// @return the file name that created an AST node
        const String& fileName() const { return m_fileName; }
        /// @return the line where the node of the AST was found
        int line() const { return m_line; }
      private:
        String m_fileName;
        int m_line;
    };
  }
}

#endif
