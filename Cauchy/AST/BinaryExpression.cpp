/*
 *  Copyright (c) 2008,2009,2010 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "BinaryExpression.h"

#include <Cauchy/Debug.h>
#include <Cauchy/Variable.h>

#include "Expression.h"
#include "GenerationVisitor.h"

using namespace Cauchy;
using namespace AST;

BinaryExpression::BinaryExpression( Expression* lhs, Expression* rhs) : m_lhs(lhs), m_rhs(rhs)
{
  CAUCHY_ASSERT(m_lhs);
  CAUCHY_ASSERT(m_lhs->type());
  CAUCHY_ASSERT(m_rhs);
  CAUCHY_ASSERT(m_rhs->type());
}


BinaryExpression::~BinaryExpression()
{
  delete m_lhs;
  delete m_rhs;
}

//------- OrBinaryExpression -------

ExpressionResultSP OrBinaryExpression::generateValue( GenerationVisitor* _generationVisitor) const
{
  return _generationVisitor->generateOrExpresion( leftHandSide()->generateValue(_generationVisitor), rightHandSide()->generateValue(_generationVisitor), annotation() );
}

//------- AndBinaryExpression -------

ExpressionResultSP AndBinaryExpression::generateValue( GenerationVisitor* _generationVisitor) const
{
  return _generationVisitor->generateAndExpresion( leftHandSide()->generateValue(_generationVisitor), rightHandSide()->generateValue(_generationVisitor), annotation() );
}

//------- OrBinaryExpression -------

ExpressionResultSP EqualEqualBinaryExpression::generateValue( GenerationVisitor* _generationVisitor) const
{
  return _generationVisitor->generateEqualExpresion( leftHandSide()->generateValue(_generationVisitor), rightHandSide()->generateValue(_generationVisitor), annotation() );
}

//------- OrBinaryExpression -------

ExpressionResultSP DifferentBinaryExpression::generateValue( GenerationVisitor* _generationVisitor) const
{
  return _generationVisitor->generateDifferentExpresion( leftHandSide()->generateValue(_generationVisitor), rightHandSide()->generateValue(_generationVisitor), annotation() );
}

//------- OrBinaryExpression -------

ExpressionResultSP InferiorEqualBinaryExpression::generateValue( GenerationVisitor* _generationVisitor) const
{
  return _generationVisitor->generateInferiorEqualExpresion( leftHandSide()->generateValue(_generationVisitor), rightHandSide()->generateValue(_generationVisitor), annotation() );
}

//------- OrBinaryExpression -------

ExpressionResultSP InferiorBinaryExpression::generateValue( GenerationVisitor* _generationVisitor) const
{
  return _generationVisitor->generateInferiorExpresion( leftHandSide()->generateValue(_generationVisitor), rightHandSide()->generateValue(_generationVisitor), annotation() );
}

//------- OrBinaryExpression -------

ExpressionResultSP SupperiorEqualBinaryExpression::generateValue( GenerationVisitor* _generationVisitor) const
{
  return _generationVisitor->generateSupperiorEqualExpresion( leftHandSide()->generateValue(_generationVisitor), rightHandSide()->generateValue(_generationVisitor), annotation() );
}

//------- OrBinaryExpression -------

ExpressionResultSP SupperiorBinaryExpression::generateValue( GenerationVisitor* _generationVisitor) const
{
  return _generationVisitor->generateSupperiorExpresion( leftHandSide()->generateValue(_generationVisitor), rightHandSide()->generateValue(_generationVisitor), annotation() );
}

//------- AdditionBinaryExpression -------

ExpressionResultSP AdditionBinaryExpression::generateValue( GenerationVisitor* _generationVisitor) const
{
  return _generationVisitor->generateAdditionExpresion( leftHandSide()->generateValue(_generationVisitor), rightHandSide()->generateValue(_generationVisitor), annotation() );
}

const Type* AdditionBinaryExpression::type() const
{
  return Cauchy::Type::optype(leftHandSide()->type(), rightHandSide()->type());
}

//------- SubtractionBinaryExpression -------

ExpressionResultSP SubtractionBinaryExpression::generateValue( GenerationVisitor* _generationVisitor) const
{
  return _generationVisitor->generateSubtractionExpresion( leftHandSide()->generateValue(_generationVisitor), rightHandSide()->generateValue(_generationVisitor), annotation() );
}

const Type* SubtractionBinaryExpression::type() const
{
  return Cauchy::Type::optype(leftHandSide()->type(), rightHandSide()->type());
}

//------- MultiplicationBinaryExpression -------

ExpressionResultSP MultiplicationBinaryExpression::generateValue( GenerationVisitor* _generationVisitor) const
{
  return _generationVisitor->generateMultiplicationExpresion( leftHandSide()->generateValue(_generationVisitor), rightHandSide()->generateValue(_generationVisitor), annotation() );
}

const Type* MultiplicationBinaryExpression::type() const
{
  return Cauchy::Type::optype(leftHandSide()->type(), rightHandSide()->type());
}

//------- ElementWiseMultiplicationBinaryExpression -------

ExpressionResultSP ElementWiseMultiplicationBinaryExpression::generateValue( GenerationVisitor* _generationVisitor) const
{
  return _generationVisitor->generateElementWiseMultiplicationExpresion( leftHandSide()->generateValue(_generationVisitor), rightHandSide()->generateValue(_generationVisitor), annotation() );
}

const Type* ElementWiseMultiplicationBinaryExpression::type() const
{
  return Cauchy::Type::optype(leftHandSide()->type(), rightHandSide()->type());
}

//------- DivisionBinaryExpression -------

ExpressionResultSP DivisionBinaryExpression::generateValue( GenerationVisitor* _generationVisitor) const
{
  return _generationVisitor->generateDivisionExpresion( leftHandSide()->generateValue(_generationVisitor), rightHandSide()->generateValue(_generationVisitor), annotation() );
}

const Type* DivisionBinaryExpression::type() const
{
  return Type::optype(leftHandSide()->type(), rightHandSide()->type());
}

//------- ElementWiseDivisionBinaryExpression -------

ExpressionResultSP ElementWiseDivisionBinaryExpression::generateValue( GenerationVisitor* _generationVisitor) const
{
  return _generationVisitor->generateElementWiseDivisionExpresion( leftHandSide()->generateValue(_generationVisitor), rightHandSide()->generateValue(_generationVisitor), annotation() );
}

const Type* ElementWiseDivisionBinaryExpression::type() const
{
  return Type::optype(leftHandSide()->type(), rightHandSide()->type());
}

//------- DivisionBinaryExpression -------

ExpressionResultSP PowerBinaryExpression::generateValue( GenerationVisitor* _generationVisitor) const
{
  return _generationVisitor->generatePowerExpresion( leftHandSide()->generateValue(_generationVisitor), rightHandSide()->generateValue(_generationVisitor), annotation() );
}

const Type* PowerBinaryExpression::type() const
{
  return Type::optype(leftHandSide()->type(), rightHandSide()->type());
}

//------- ElementWiseDivisionBinaryExpression -------

ExpressionResultSP ElementWisePowerBinaryExpression::generateValue( GenerationVisitor* _generationVisitor) const
{
  return _generationVisitor->generateElementWisePowerExpresion( leftHandSide()->generateValue(_generationVisitor), rightHandSide()->generateValue(_generationVisitor), annotation() );
}

const Type* ElementWisePowerBinaryExpression::type() const
{
  return Type::optype(leftHandSide()->type(), rightHandSide()->type());
}

//------- OrBinaryExpression -------

AssignementBinaryExpression::AssignementBinaryExpression( Expression* lhs, Expression* rhs ) : BinaryExpression( lhs, rhs )
{
}

ExpressionResultSP AssignementBinaryExpression::generateValue( GenerationVisitor* _generationVisitor) const
{
  ExpressionResultSP asg = rightHandSide()->generateValue(_generationVisitor);
  return _generationVisitor->generateAssignementExpression( leftHandSide()->generateValue(_generationVisitor), asg, annotation());
}

const Type* AssignementBinaryExpression::type() const
{
  return rightHandSide()->type();
}
