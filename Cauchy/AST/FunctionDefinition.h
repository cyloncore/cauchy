/*
 *  Copyright (c) 2010 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include <Cauchy/String.h>

namespace Cauchy {
  class FunctionDeclaration;
  class Variable;
  namespace AST {
    class Statement;
    class GenerationVisitor;
    /**
     * @internal
     * This class contains a function definition.
     * @ingroup Cauchy_AST
     */
    class FunctionDefinition {
      public:
        FunctionDefinition(const Cauchy::FunctionDeclaration* declaration, const std::vector<String>& returns, const std::vector<String>& arguments );
        ~FunctionDefinition();
        void append(AST::Statement* statement);
        void generate( GenerationVisitor* _generationVisitor );
        String name() const;
        const std::vector<String>& arguments() const;
        const std::vector<String>& returns() const;
        void setReturnVariables(const std::vector<Variable*>& _returns);
        const std::vector<Variable*>& returnVariables() const;
        const Cauchy::FunctionDeclaration* declaration() const;
      private:
        struct Private;
        Private* const d;
    };
  }
}
