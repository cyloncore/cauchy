/*
 *  Copyright (c) 2008 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "UnaryExpression.h"

#include "GenerationVisitor.h"
#include "../Debug.h"

using namespace Cauchy;
using namespace AST;

//--------- UnaryExpression ---------//

UnaryExpression::~UnaryExpression()
{
  delete m_rhs;
}

//--------- MinusUnaryExpression ---------//

ExpressionResultSP MinusUnaryExpression::generateValue( GenerationVisitor* _generationVisitor) const
{
  return _generationVisitor->generateMinusExpression( rightHandSide()->generateValue(_generationVisitor), annotation());
}

//--------- MinusMinusUnaryExpression ---------//

ExpressionResultSP MinusMinusUnaryExpression::generateValue( GenerationVisitor* /*_generationVisitor*/) const
{
  CAUCHY_ABORT("Unimplemented");
  return 0;
}

//--------- PlusPlusUnaryExpression ---------//

ExpressionResultSP PlusPlusUnaryExpression::generateValue( GenerationVisitor* /*_generationVisitor*/) const
{
  CAUCHY_ABORT("Unimplemented");
  return 0;
}

//--------- TildeUnaryExpression ---------//

ExpressionResultSP NotUnaryExpression::generateValue( GenerationVisitor* _generationVisitor) const
{
  return _generationVisitor->generateNotExpression( rightHandSide()->generateValue(_generationVisitor), annotation());
}

//--------- TildeUnaryExpression ---------//

ExpressionResultSP TildeUnaryExpression::generateValue( GenerationVisitor* _generationVisitor) const
{
  return _generationVisitor->generateTildExpression( rightHandSide()->generateValue(_generationVisitor), annotation());
}

ExpressionResultSP TransposeUnaryExpression::generateValue( GenerationVisitor* _generationVisitor) const
{
  return _generationVisitor->generateTransposeExpression( rightHandSide()->generateValue(_generationVisitor), annotation());  
}
