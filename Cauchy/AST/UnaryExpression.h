/*
 *  Copyright (c) 2008 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef _UNARY_EXPRESSION_H_
#define _UNARY_EXPRESSION_H_

#include "Cauchy//AST/Expression.h"

namespace Cauchy {

  namespace AST {

    /**
     * @internal
     * This is the base class for unary expressions ('!', '-', '++' ...)
     * @ingroup Cauchy_AST
     */
    class UnaryExpression : public Expression {
      public:
        explicit UnaryExpression(Expression* rhs) : m_rhs(rhs) {}
        ~UnaryExpression();
        const Expression* rightHandSide() const { return m_rhs; }
        virtual const Type* type() const { return m_rhs->type(); }
      private:
        Expression* m_rhs;
    };
    /**
     * @internal
     * @ingroup Cauchy_AST
     */
    class MinusUnaryExpression : public UnaryExpression {
      public:
        explicit MinusUnaryExpression(Expression* rhs) : UnaryExpression(rhs) {}
        virtual ExpressionResultSP generateValue( GenerationVisitor* _generationVisitor) const;
    };
    /**
     * @internal
     * @ingroup Cauchy_AST
     */
    class MinusMinusUnaryExpression : public UnaryExpression {
      public:
        explicit MinusMinusUnaryExpression(Expression* rhs) : UnaryExpression(rhs) {}
        virtual ExpressionResultSP generateValue( GenerationVisitor* _generationVisitor) const;
    };
    /**
     * @internal
     * @ingroup Cauchy_AST
     */
    class PlusPlusUnaryExpression : public UnaryExpression {
      public:
        explicit PlusPlusUnaryExpression(Expression* rhs) : UnaryExpression(rhs) {}
        virtual ExpressionResultSP generateValue( GenerationVisitor* _generationVisitor) const;
    };
    /**
     * @internal
     * @ingroup Cauchy_AST
     */
    class NotUnaryExpression : public UnaryExpression {
      public:
        explicit NotUnaryExpression(Expression* rhs) : UnaryExpression(rhs) {}
        virtual ExpressionResultSP generateValue( GenerationVisitor* _generationVisitor) const;
    };
    /**
     * @internal
     * @ingroup Cauchy_AST
     */
    class TildeUnaryExpression : public UnaryExpression {
      public:
        explicit TildeUnaryExpression(Expression* rhs) : UnaryExpression(rhs) {}
        virtual ExpressionResultSP generateValue( GenerationVisitor* _generationVisitor) const;
    };
    /**
     * @internal
     * @ingroup Cauchy_AST
     */
    class TransposeUnaryExpression : public UnaryExpression {
      public:
        explicit TransposeUnaryExpression(Expression* rhs) : UnaryExpression(rhs) {}
        virtual ExpressionResultSP generateValue( GenerationVisitor* _generationVisitor) const;
    };    
  }
}

#endif
