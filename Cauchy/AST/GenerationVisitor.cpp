/*
 *  Copyright (c) 2009 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "GenerationVisitor.h"
#include "Expression.h"

#include "Cauchy/CompilationMessage.h"
#include "Cauchy/CompilationMessages.h"
#include "Cauchy/Macros_p.h"
#include <Cauchy/CompilationMessages_p.h>

using namespace Cauchy;
using namespace AST;

struct GenerationVisitor::Private {
  Cauchy::CompilationMessages errorMessages;
};

GenerationVisitor::GenerationVisitor() : d(new Private)
{
}

GenerationVisitor::GenerationVisitor(const GenerationVisitor& ) : d(0)
{
}

GenerationVisitor& GenerationVisitor::operator=(const GenerationVisitor& )
{
  return *this;
}

GenerationVisitor::~GenerationVisitor()
{
  delete d;
}

ExpressionResultSP GenerationVisitor::generateFunctionCall(const String& _function, const Cauchy::FunctionDeclaration* declaration, const Variable* variable, const std::list<Expression*>& _arguments, const std::vector<AST::Expression*>& _returns, const Annotation& _annotation)
{
  std::list<ExpressionResultSP> arguments;
  foreach(Expression* expr, _arguments)
  {
    arguments.push_back(expr->generateValue(this));
  }
  std::vector<ExpressionResultSP> returns;
  foreach(Expression* expr, _returns)
  {
    returns.push_back(expr->generateValue(this));
  }
  return generateFunctionCall(_function, declaration, variable, arguments, returns, _annotation);
}

void GenerationVisitor::reportError(const String& _message, const Annotation& _annotation)
{
  d->errorMessages.d->appendMessage( CompilationMessage(CompilationMessage::ERROR, _message, _annotation.line(), _annotation.fileName() ) );
}

CompilationMessages GenerationVisitor::generationMessages()
{
  return d->errorMessages;
}
