/*
 *  Copyright (c) 2010 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "FunctionDefinition.h"
#include "GenerationVisitor.h"

#include "../Macros_p.h"
#include "Statement.h"
#include "../FunctionDeclaration.h"

using namespace Cauchy::AST;

struct FunctionDefinition::Private {
  std::list<Statement*> main;
  std::vector<String> arguments;
  std::vector<String> returns;
  std::vector<Variable*> returnsVariable;
  const Cauchy::FunctionDeclaration* declaration;
};

FunctionDefinition::FunctionDefinition(const Cauchy::FunctionDeclaration* declaration, const std::vector< Cauchy::String >& returns, const std::vector< Cauchy::String >& arguments) : d(new Private)
{
  d->declaration = declaration;
  d->arguments = arguments;
  d->returns = returns;
}

FunctionDefinition::~FunctionDefinition()
{
}

void FunctionDefinition::append(Statement* statement)
{
  d->main.push_back(statement);
}

void FunctionDefinition::generate( GenerationVisitor* _generationVisitor )
{
  _generationVisitor->startFunction(this);
  foreach(Statement* statement, d->main)
  {
    statement->generateStatement(_generationVisitor);
  }
}

Cauchy::String FunctionDefinition::name() const
{
  return d->declaration->name();
}

const std::vector<Cauchy::String>& FunctionDefinition::arguments() const
{
  return d->arguments;
}

const std::vector<Cauchy::String>& FunctionDefinition::returns() const
{
  return d->returns;
}

const Cauchy::FunctionDeclaration* FunctionDefinition::declaration() const
{
  return d->declaration;
}

const std::vector< Cauchy::Variable* >& FunctionDefinition::returnVariables() const
{
  return d->returnsVariable;
}

void FunctionDefinition::setReturnVariables(const std::vector< Cauchy::Variable* >& _returns)
{
  d->returnsVariable = _returns;
}
