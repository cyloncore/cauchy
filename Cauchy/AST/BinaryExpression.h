/*
 *  Copyright (c) 2008,2010 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef _BINARY_EXPRESSION_H_
#define _BINARY_EXPRESSION_H_

#include "Cauchy/AST/Expression.h"

namespace Cauchy {

  namespace AST {
    /**
     * @internal
     * This is the base class of binary expressions ( '<', '==', '+', '%'...).
     * @ingroup Cauchy_AST
     */
    class BinaryExpression : public Expression {
      public:
        BinaryExpression( Expression* lhs, Expression* rhs);
        ~BinaryExpression();
        int priority();
        Expression* leftHandSide() { return m_lhs; }
        const Expression* leftHandSide() const { return m_lhs; }
        Expression* rightHandSide() { return m_rhs; }
        const Expression* rightHandSide() const { return m_rhs; }
      private:
        Expression* m_lhs;
        Expression* m_rhs;
    };
    /**
     * @internal
     * @ingroup Cauchy_AST
     */
    class OrBinaryExpression : public BinaryExpression {
      public:
        OrBinaryExpression( Expression* lhs, Expression* rhs ) : BinaryExpression( lhs, rhs )
        {
        }
        virtual ExpressionResultSP generateValue( GenerationVisitor* _generationVisitor) const;
        virtual const Type* type() const { return Type::Logical; }
    };
    /**
     * @internal
     * @ingroup Cauchy_AST
     */
    class AndBinaryExpression : public BinaryExpression {
      public:
        AndBinaryExpression( Expression* lhs, Expression* rhs ) : BinaryExpression( lhs, rhs )
        {
        }
        virtual ExpressionResultSP generateValue( GenerationVisitor* _generationVisitor) const;
        virtual const Type* type() const { return Type::Logical; }
    };
    /**
     * @internal
     * @ingroup Cauchy_AST
     */
    class EqualEqualBinaryExpression : public BinaryExpression {
      public:
        EqualEqualBinaryExpression( Expression* lhs, Expression* rhs ) : BinaryExpression( lhs, rhs )
        {
        }
        virtual ExpressionResultSP generateValue( GenerationVisitor* _generationVisitor) const;
        virtual const Type* type() const { return Type::Logical; }
    };
    /**
     * @internal
     * @ingroup Cauchy_AST
     */
    class DifferentBinaryExpression : public BinaryExpression {
      public:
        DifferentBinaryExpression( Expression* lhs, Expression* rhs ) : BinaryExpression( lhs, rhs )
        {
        }
        virtual ExpressionResultSP generateValue( GenerationVisitor* _generationVisitor) const;
        virtual const Type* type() const { return Type::Logical; }
    };
    /**
     * @internal
     * @ingroup Cauchy_AST
     */
    class InferiorEqualBinaryExpression : public BinaryExpression {
      public:
        InferiorEqualBinaryExpression( Expression* lhs, Expression* rhs ) : BinaryExpression( lhs, rhs )
        {
        }
        virtual ExpressionResultSP generateValue( GenerationVisitor* _generationVisitor) const;
        virtual const Type* type() const { return Type::Logical; }
    };
    /**
     * @internal
     * @ingroup Cauchy_AST
     */
    class InferiorBinaryExpression : public BinaryExpression {
      public:
        InferiorBinaryExpression( Expression* lhs, Expression* rhs ) : BinaryExpression( lhs, rhs )
        {
        }
        virtual ExpressionResultSP generateValue( GenerationVisitor* _generationVisitor) const;
        virtual const Type* type() const { return Type::Logical; }
    };
    /**
     * @internal
     * @ingroup Cauchy_AST
     */
    class SupperiorEqualBinaryExpression : public BinaryExpression {
      public:
        SupperiorEqualBinaryExpression( Expression* lhs, Expression* rhs ) : BinaryExpression( lhs, rhs )
        {
        }
        virtual ExpressionResultSP generateValue( GenerationVisitor* _generationVisitor) const;
        virtual const Type* type() const { return Type::Logical; }
    };
    /**
     * @internal
     * @ingroup Cauchy_AST
     */
    class SupperiorBinaryExpression : public BinaryExpression {
      public:
        SupperiorBinaryExpression( Expression* lhs, Expression* rhs ) : BinaryExpression( lhs, rhs )
        {
        }
        virtual ExpressionResultSP generateValue( GenerationVisitor* _generationVisitor) const;
        virtual const Type* type() const { return Type::Logical; }
    };
    /**
     * @internal
     * @ingroup Cauchy_AST
     */
    class AdditionBinaryExpression : public BinaryExpression {
      public:
        AdditionBinaryExpression( Expression* lhs, Expression* rhs ) : BinaryExpression( lhs, rhs )
        {
        }
        virtual ExpressionResultSP generateValue( GenerationVisitor* _generationVisitor) const;
        virtual const Type* type() const;
    };
    /**
     * @internal
     * @ingroup Cauchy_AST
     */
    class SubtractionBinaryExpression : public BinaryExpression {
      public:
        SubtractionBinaryExpression( Expression* lhs, Expression* rhs ) : BinaryExpression( lhs, rhs )
        {
        }
        virtual ExpressionResultSP generateValue( GenerationVisitor* _generationVisitor) const;
        virtual const Type* type() const;
    };
    /**
     * @internal
     * @ingroup Cauchy_AST
     */
    class MultiplicationBinaryExpression : public BinaryExpression {
      public:
        MultiplicationBinaryExpression( Expression* lhs, Expression* rhs ) : BinaryExpression( lhs, rhs )
        {
        }
        virtual ExpressionResultSP generateValue( GenerationVisitor* _generationVisitor) const;
        virtual const Type* type() const;
    };
    /**
     * @internal
     * @ingroup Cauchy_AST
     */
    class ElementWiseMultiplicationBinaryExpression : public BinaryExpression {
      public:
        ElementWiseMultiplicationBinaryExpression( Expression* lhs, Expression* rhs ) : BinaryExpression( lhs, rhs )
        {
        }
        virtual ExpressionResultSP generateValue( GenerationVisitor* _generationVisitor) const;
        virtual const Type* type() const;
    };
    /**
     * @internal
     * @ingroup Cauchy_AST
     */
    class DivisionBinaryExpression : public BinaryExpression {
      public:
        DivisionBinaryExpression ( Expression* lhs, Expression* rhs ) : BinaryExpression( lhs, rhs )
        {
        }
        virtual ExpressionResultSP generateValue( GenerationVisitor* _generationVisitor) const;
        virtual const Type* type() const;
    };
    /**
     * @internal
     * @ingroup Cauchy_AST
     */
    class ElementWiseDivisionBinaryExpression : public BinaryExpression {
      public:
        ElementWiseDivisionBinaryExpression ( Expression* lhs, Expression* rhs ) : BinaryExpression( lhs, rhs )
        {
        }
        virtual ExpressionResultSP generateValue( GenerationVisitor* _generationVisitor) const;
        virtual const Type* type() const;
    };
    /**
     * @internal
     * @ingroup Cauchy_AST
     */
    class PowerBinaryExpression : public BinaryExpression {
      public:
        PowerBinaryExpression( Expression* lhs, Expression* rhs ) : BinaryExpression( lhs, rhs )
        {
        }
        virtual ExpressionResultSP generateValue( GenerationVisitor* _generationVisitor) const;
        virtual const Type* type() const;
    };
    /**
     * @internal
     * @ingroup Cauchy_AST
     */
    class ElementWisePowerBinaryExpression : public BinaryExpression {
      public:
        ElementWisePowerBinaryExpression( Expression* lhs, Expression* rhs ) : BinaryExpression( lhs, rhs )
        {
        }
        virtual ExpressionResultSP generateValue( GenerationVisitor* _generationVisitor) const;
        virtual const Type* type() const;
    };
    /**
     * @internal
     * This is the class representing an assignement of a value to a variable, the left hand side of this
     * expression must be a @ref VariableExpression.
     * @ingroup Cauchy_AST
     */
    class AssignementBinaryExpression : public BinaryExpression {
      public:
        AssignementBinaryExpression( Expression* lhs, Expression* rhs );
        virtual ExpressionResultSP generateValue( GenerationVisitor* _generationVisitor) const;
        virtual const Type* type() const;
    };
  }
}

#endif
