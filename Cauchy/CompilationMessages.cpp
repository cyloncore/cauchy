/*
 *  Copyright (c) 2009 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "CompilationMessages.h"

#include <sstream>

#include "CompilationMessages_p.h"
#include "CompilationMessage.h"
#include "Macros_p.h"

using namespace Cauchy;

CompilationMessages::CompilationMessages() : d(new Private)
{
}

CompilationMessages::CompilationMessages(const CompilationMessages& _rhs) : d(new Private)
{
  *this = _rhs;
}

CompilationMessages& CompilationMessages::operator=(const CompilationMessages& _rhs)
{
  *d = *_rhs.d;
  return *this;
}

CompilationMessages::~CompilationMessages()
{
  delete d;
}

const std::list<CompilationMessage>& CompilationMessages::errors() const
{
  return d->errors;
}

const std::list<CompilationMessage>& CompilationMessages::warnings() const
{
  return d->warnings;
}

const std::list<CompilationMessage>& CompilationMessages::messages() const
{
  return d->warnings;
}

void CompilationMessages::Private::appendMessage(const CompilationMessage& _message)
{
  messages.push_back(_message);
  switch(_message.type())
  {
    case CompilationMessage::ERROR:
      errors.push_back(_message);
      break;
    case CompilationMessage::WARNING:
      warnings.push_back(_message);
      break;
  }
}

String CompilationMessages::toString() const {
  std::ostringstream os;
  foreach(CompilationMessage msg, d->messages)
  {
    switch(msg.type())
    {
      case CompilationMessage::ERROR:
        os << "Error: ";
        break;
      case CompilationMessage::WARNING:
        os << "Warning: ";
        break;
    }
    os <<  msg.fileName() << " at " << msg.line() << " : " << msg.message()  << std::endl;
  }
  return os.str();
}
