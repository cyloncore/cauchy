/*
 *  Copyright (c) 2010 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef _CAUCHY_FUCTIONS_DEFINITIONS_REGISTRY_H_
#define _CAUCHY_FUCTIONS_DEFINITIONS_REGISTRY_H_

#include <list>
#include <vector>
#include <Cauchy/Type.h>

namespace Cauchy {
  class String;
  class FunctionDeclaration;
  class StructureDeclaration;
  class VariableDeclaration;
  /**
   * This class contains the definition of globals, constants, functions and structures
   * defined in CFD files.
   * @ingroup Cauchy
   */
  class DeclarationsRegistry {
    public:
      DeclarationsRegistry();
      ~DeclarationsRegistry();
      void addSearchPath(const String& path);
      /**
       * Load information from the given file.
       * @return true if it was successfully loaded
       */
      bool load(const String& file);
      /**
       * @return all the definitions corresponding to that name
       */
      std::vector<const FunctionDeclaration*> function(const String& functionName) const;
      /**
       * @param returns the number of return expression
       * @return the function declaration that is closest given the arguments, or none
       */
      const FunctionDeclaration* function(const Cauchy::String& functionName, const std::vector< const Type* >& arguments, int returns ) const;
      /**
       * @param returns the number of return expression
       * @return the function declaration that is closest given the arguments, or none
       */
      const FunctionDeclaration* function(const Cauchy::String& functionName, int arguments, int returns ) const;
      /**
       * @return the type of a constant 'name'
       */
      const VariableDeclaration* constant(const String& name) const;
      /**
       * @return the type of a global 'name'
       */
      const VariableDeclaration* global(const String& name) const;
      /**
       * @return a structure for the given name
       */
      const StructureDeclaration* structure(const String& name) const;
      /**
       * @return all functions
       */
      std::list<const FunctionDeclaration*> functions() const;
      /**
       * @return all constants
       */
      std::list<const VariableDeclaration*> constants() const;
      /**
       * @return all globals
       */
      std::list<const VariableDeclaration*> globals() const;
    private:
      struct Private;
      Private* const d;
  };
}

#endif
