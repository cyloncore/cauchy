/*
 *  Copyright (c) 2010 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "DeclarationsGenerator.h"

#include "Debug.h"

#ifndef NO_EIGEN_BACKEND
#include "EigenBackend/DeclarationsGenerator.h"
#endif

#define C_UNUSED(x) (void)x;

using namespace Cauchy;

DeclarationsGenerator::DeclarationsGenerator() : d(0)
{
}

DeclarationsGenerator::~DeclarationsGenerator()
{
//   delete d;
}


String DeclarationsGenerator::generateDeclarations(const DeclarationsRegistry* registry, const Options& options, const String& backend )
{
#ifndef NO_EIGEN_BACKEND
  CAUCHY_ASSERT(backend == "eigen3");
  C_UNUSED(backend) // For when the assert is disabled
  EigenBackend::DeclarationsGenerator dg;
  return dg.generate(registry, options);
#else
  C_UNUSED(registry)
  C_UNUSED(options)
  C_UNUSED(backend)
  return "";
#endif
}
