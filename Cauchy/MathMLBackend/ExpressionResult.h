/*
 *  Copyright (c) 2011 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef _EIGENBACKEND_EXPRESSION_RESULT_H_
#define _EIGENBACKEND_EXPRESSION_RESULT_H_

#include "Cauchy/AST/ExpressionResult.h"
#include <Cauchy/String.h>
#include <Cauchy/Type.h>

namespace MathMLBackend {
  class ExpressionResult : public Cauchy::AST::ExpressionResult {
    public:
      ExpressionResult(const Cauchy::String& result);
      virtual ~ExpressionResult();
      Cauchy::String result() const;
    private:
      Cauchy::String m_result;
  };
}

#endif
