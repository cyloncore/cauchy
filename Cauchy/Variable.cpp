/*
 *  Copyright (c) 2010 Cyrille Berger <cberger@cberger.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "Variable.h"

#include "Debug.h"
#include "VariableDeclaration.h"

using namespace Cauchy;

Variable::Variable(const Cauchy::String& name, Variable::Qualifier qualifier) : m_name(name), m_type(Type::Unknown), m_qualifier(qualifier), m_declaration(0)
{
}

Variable::Variable(const VariableDeclaration* declaration) : m_declaration(declaration)
{
  if(m_declaration->isConstant())
  {
    m_qualifier = Constant;
  } else {
    m_qualifier = Global;
  }
  m_type = m_declaration->type();
  m_name = m_declaration->name();
}

Cauchy::String Variable::name() const
{
  return m_name;
}

const Type* Variable::type() const
{
  return m_type;
}

bool Variable::setType(const Type* type)
{
  if(m_type == Type::Unknown or type == m_type) {
    m_type = type;
    return true;
  } else {
    if(m_type->isMatrix() and type->isMatrix())
    {
      if(m_type->embeddedType()->isComplex())
      {
        return m_type->embeddedType()->embeddedType() == type->embeddedType();
      } else if(type->embeddedType()->isComplex()) {
        if(m_type->embeddedType() == type->embeddedType()->embeddedType())
        {
          m_type = type;
          return true;
        }
      }
      return false;
    } else {
    }
    return false;
  }
}

Variable::Qualifier Variable::qualifier() const
{
  return m_qualifier;
}

const VariableDeclaration* Variable::declaration() const
{
  return m_declaration;
}

bool Variable::setPersistent()
{
  if(m_qualifier == Normal)
  {
    m_qualifier = Persistent;
    return true;
  } else {
    return false;
  }
}
